import os
import tempfile
import unittest

from protqc.analyzer import QCAnalyzer
from protqc.json_utils import compare_qc_data_json_files
from protqc.modules import MassDeviationComponent
from protqc.reporter import QCReporter
from protqc.settings import PROJECT_ROOT

QC_DATA_DOWN_10000_MZML_DIR = os.path.join(PROJECT_ROOT, 'test-data', 'datasets', 'down10000', 'mzml')
QC_DATA_DOWN_10000_JSON_DIR = os.path.join(PROJECT_ROOT, 'test-data', 'datasets', 'down10000', 'json')
QC_DATA_DOWN_10000_ID_JSON_DIR = os.path.join(PROJECT_ROOT, 'test-data', 'datasets', 'down10000', 'id-json')
QC_DATA_DOWN_10000_ALL_SAMPLES_EXPECTED_OUTPUT = os.path.join(PROJECT_ROOT, 'test-data', 'expected-output', 'down10000',
                                                              'all_samples_report')
QC_DATA_DOWN_10000_BATCH_EXPECTED_OUTPUT = os.path.join(PROJECT_ROOT, 'test-data', 'expected-output', 'down10000',
                                                        'batch_report')


class QCDataTest(unittest.TestCase):
    def setUp(self):
        self.temp_output_dir = tempfile.mkdtemp(prefix='qc-report.')

    def test_down_10000_analyze(self):
        self._compare_json(source=QC_DATA_DOWN_10000_MZML_DIR, dest=QC_DATA_DOWN_10000_JSON_DIR)

    def test_down_10000_id_json_load(self):
        self._compare_json(source=QC_DATA_DOWN_10000_ID_JSON_DIR, id_dir=os.path.join(QC_DATA_DOWN_10000_ID_JSON_DIR, 'id_dir'))

    def test_down_10000_json_load(self):
        self._compare_json(QC_DATA_DOWN_10000_JSON_DIR)

    def test_down_10000_all_samples_report(self):
        self._compare_output()

    def test_down_10000_batch_report(self):
        self._compare_output()

    def _anaylize(self, source, output, id_dir=None):
        qc_analyzer = QCAnalyzer(source, output, id_dir, masses=MassDeviationComponent.KNOWN_MASSES)
        return qc_analyzer.analyze()

    def _compare_output(self):
        qc_data = self._anaylize(QC_DATA_DOWN_10000_JSON_DIR, self.temp_output_dir)
        qc_reporter = QCReporter(qc_data, self.temp_output_dir)
        qc_reporter.create_qc_report(batch=True)
        expected_output_files = [os.path.join(os.path.relpath(dp, QC_DATA_DOWN_10000_BATCH_EXPECTED_OUTPUT), filename)
                                 for dp, _, filenames in os.walk(QC_DATA_DOWN_10000_BATCH_EXPECTED_OUTPUT) for filename
                                 in filenames]
        test_output_files = [os.path.join(os.path.relpath(dp, self.temp_output_dir), filename) for dp, _, filenames in
                             os.walk(self.temp_output_dir) for filename in filenames]
        self.assertEqual(set(test_output_files), set(expected_output_files))

    def _compare_json(self, source, dest=None, id_dir=None):
        if not dest:
            dest=source
        temp_output_dir = tempfile.mkdtemp(dir=self.temp_output_dir, prefix='compare_json.')
        qc_data = self._anaylize(source, temp_output_dir, id_dir=id_dir)

        # compare jsons files:
        for file in os.listdir(temp_output_dir):
            if file.endswith('.json'):
                json_path1 = os.path.join(temp_output_dir, file)
                json_path2 = os.path.join(dest, file)
                self.assertTrue(compare_qc_data_json_files(json_path1, json_path2),
                                'json files are not almost equal: %s, %s'
                                % (json_path1, json_path2))
