#!/usr/bin/env python
import argparse
import sys
from protqc.runner import QCRunner
from protqc.modules import MassDeviationComponent


DEFAULT_NUM_PROCESSES = 1


def parse_args():
    parser = argparse.ArgumentParser(description="Creates a Proteomics QC Report")
    parser.add_argument('--input', required=True, nargs='+', type=str, help='mzML/raw/d/wiff files or folders that contain the previous formats separated by space.')
    parser.add_argument('--id-dir', required=False)
    parser.add_argument('--output-dir', type=str, required=True, help='Output folder, were the report will be written.')
    parser.add_argument('--cores', type=int, default=DEFAULT_NUM_PROCESSES, help='Number of processes to use, Default: %d' % DEFAULT_NUM_PROCESSES)
    parser.add_argument('--batch', action='store_true', help='Toggle QC Report mode, if True - one report for each sample, if False - one reports to all samples. Default: off')
    parser.add_argument('--no-report', action='store_true', help='Don\'t generate report, only jsons. Default: off')
    parser.add_argument('--masses', nargs='+', type=float, default=MassDeviationComponent.KNOWN_MASSES, help='List of masses to follow, separated by space. Default: [371.10124, 445.12003]')
    return parser.parse_args()


def main():
    args = parse_args()
    cmd = ' '.join(sys.argv)
    qc_runner = QCRunner(args.input, args.output_dir, num_processes=args.cores, id_dir=args.id_dir, batch=args.batch, masses=args.masses, no_report=args.no_report, cmd=cmd)
    qc_runner.run()


if __name__ == '__main__':
    main()
