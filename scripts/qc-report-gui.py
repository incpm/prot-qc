#!/usr/bin/env python
from protqc.settings import set_is_gui
set_is_gui(True)

import multiprocessing
import traceback
import os
import webbrowser
import wx
from threading import Thread
import psutil

from protqc.runner import QCRunner, get_help_bmp
from protqc.modules import MassDeviationComponent

"""logging:"""
import logging
logger = logging.getLogger(__name__)


FILES_DELIMITER = ';'
END_PROCESS = "END!"


class QueueLoggerHandler(logging.Handler):
    def __init__(self, queue):
        logging.Handler.__init__(self)
        self.queue = queue

    def emit(self, record):
        s = self.format(record) + '\n'
        # print('test: ' + s)
        self.queue.put(s)


def run_qc_process(queue, **kwargs):
    try:
        # print('protqc process pid, on start: ' + str(os.getpid()))
        # set logger:
        handler = QueueLoggerHandler(queue)
        FORMAT = "%(asctime)s - [%(levelname)s] - %(name)s - %(message)s"
        handler.setFormatter(logging.Formatter(FORMAT))
        handler.setLevel(logging.INFO)

        # start running protqc:
        qc_runner = QCRunner(**kwargs, logger_handlers=[handler])
        qc_runner.run()
    except Exception:
        tb = traceback.format_exc()
        queue.put(tb)
    finally:
        queue.put(END_PROCESS)


class QCReporterGUI(wx.Frame):
    PARAMS_PATH = 'qc.params'
    HELP_URL = 'https://incpmpm.atlassian.net/wiki/spaces/PUB/pages/103290936/Proteomics+QC+Reporter'

    def __init__(self, parent, title):
        super(QCReporterGUI, self).__init__(parent, title=title,
                                          size=(500, 690))
        self.init_ui()
        self.Centre()
        self.Show()
        self.Bind(wx.EVT_CLOSE,self.on_close)
        self.running = False
        self.qc_thread = None
        self.p = None
        self.msconvert_exe.SetValue(self.get_msconvert_path())
        self.on_exit = False

        # save gui child processes:
        parent = psutil.Process(os.getpid())
        self.gui_children_pids = [child.pid for child in parent.children(recursive=True)]
        # print('gui children:' + str(self.gui_children_pids))

    def init_ui(self):
        # Add a panel so it looks correct on all platforms
        self.panel = wx.Panel(self, wx.ID_ANY)

        # title:
        title_sizer = wx.BoxSizer(wx.HORIZONTAL)
        font = wx.Font(12, wx.SWISS, wx.NORMAL, wx.BOLD)
        title = wx.StaticText(self.panel, wx.ID_ANY, 'Raw Beans')
        title.SetFont(font)
        title_sizer.Add(title, 0, wx.ALL, 10)

        # help button:
        help_bmp = get_help_bmp()
        self.help_button = wx.BitmapButton(self.panel, id=wx.ID_ANY, bitmap=help_bmp, pos=(453, 1),
                                           size=(help_bmp.GetWidth() + 7, help_bmp.GetHeight() + 7))
        self.help_button.Bind(wx.EVT_BUTTON, self.on_help_clicked)
        self.help_button.SetLabel("HELP!")

        # title_sizer.Add(self.help_button, 0, wx.ALL | wx.ALIGN_RIGHT, 10)

        # choose msconvert.exe file:
        msconvert_select_sizer = wx.BoxSizer(wx.HORIZONTAL)
        msconvert_file_label = wx.StaticText(self.panel, wx.ID_ANY, 'ProteoWizard msconvert.exe')
        self.msconvert_button = wx.Button(self.panel, label='Browse', size=(85, 30))
        self.msconvert_button.Bind(wx.EVT_BUTTON, lambda event: self.on_open(event, self.msconvert_exe))
        self.msconvert_exe = wx.TextCtrl(self.panel, wx.ID_ANY, '', size=(-1, 23))
        msconvert_select_sizer.Add(self.msconvert_exe, 1, wx.BOTTOM | wx.TOP | wx.RIGHT, 5)
        msconvert_select_sizer.Add(self.msconvert_button, 0, wx.LEFT, 0)

        # input dir:
        input_dir_label = wx.StaticText(self.panel, wx.ID_ANY, 'Input Directory:')
        self.input_dir = wx.DirPickerCtrl(self.panel, message='Please select an input directory', size=(300, 30), path="")

        # output dir:
        output_dir_label = wx.StaticText(self.panel, wx.ID_ANY, 'Output Directory:')
        self.output_dir = wx.DirPickerCtrl(self.panel, message='Please select an output directory',  size=(300, 30), path="")

        # mass list:
        masses_list_label = wx.StaticText(self.panel, wx.ID_ANY, 'Masses List (one mass per line):')
        self.masses_list = wx.TextCtrl(self.panel, -1, style=wx.TE_MULTILINE | wx.BORDER_SUNKEN | wx.TE_RICH2, size=(450, 70))
        self.masses_list.SetValue('\n'.join([str(mass) for mass in MassDeviationComponent.KNOWN_MASSES]) + '\n')

        files_sizer = wx.BoxSizer(wx.VERTICAL)
        files_sizer.Add(msconvert_file_label, 0,wx.RIGHT| wx.LEFT | wx.TOP | wx.EXPAND, 10)
        files_sizer.Add(msconvert_select_sizer, 0, wx.ALL | wx.EXPAND, 10)
        files_sizer.Add(input_dir_label, 0, wx.RIGHT| wx.LEFT | wx.TOP | wx.EXPAND, 10)
        files_sizer.Add(self.input_dir, 0, wx.ALL | wx.EXPAND, 10)
        files_sizer.Add(output_dir_label, 0, wx.RIGHT| wx.LEFT | wx.TOP |wx.EXPAND, 10)
        files_sizer.Add(self.output_dir, 0, wx.ALL|wx.EXPAND, 10)
        files_sizer.Add(masses_list_label, 0, wx.RIGHT| wx.LEFT | wx.TOP | wx.EXPAND, 10)
        files_sizer.Add(self.masses_list, 0, wx.ALL|wx.EXPAND, 10)

        # parameters:
        parameters_sizer = wx.GridBagSizer(hgap=2, vgap=1)
        self.batch_check_box = wx.CheckBox(self.panel, label='Batch')
        self.no_report_check_box = wx.CheckBox(self.panel, label='No Report')
        num_processes_label = wx.StaticText(self.panel, wx.ID_ANY, 'Num Cores:')
        self.num_processes_text_box = wx.TextCtrl(self.panel, wx.ID_ANY, '1')

        parameters_sizer.Add(self.batch_check_box, pos=(0, 0), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=10)
        parameters_sizer.Add(self.no_report_check_box, pos=(0, 1), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=10)
        parameters_sizer.Add(num_processes_label, pos=(1, 0), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=10)
        parameters_sizer.Add(self.num_processes_text_box, pos=(1, 1), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=10)

        # run protqc button:
        self.run_qc_button = wx.Button(self.panel, label='Run QC')
        self.Bind(wx.EVT_BUTTON, self.run_qc_on_click, self.run_qc_button)

        # text area for logging:
        self.logger = wx.TextCtrl(self.panel, -1, style=wx.TE_MULTILINE|wx.BORDER_SUNKEN|wx.TE_READONLY|
                                wx.TE_RICH2, size=(450,200))

        top_sizer = wx.BoxSizer(wx.VERTICAL)
        top_sizer.Add(title_sizer, 0, wx.CENTER, 5)
        top_sizer.Add(files_sizer,0, wx.EXPAND, 5)
        top_sizer.Add(parameters_sizer, 0, wx.LEFT, 5)
        top_sizer.Add(self.logger, 1, wx.CENTER|wx.TOP|wx.RIGHT|wx.LEFT|wx.EXPAND, 10)
        top_sizer.Add(self.run_qc_button, 0, wx.CENTER | wx.BOTTOM | wx.TOP, 15)

        self.panel.SetSizerAndFit(top_sizer)

    def on_open(self, event, text_field):
        open_file_dialog = wx.FileDialog(self, "Please select the msconvert.exe file in the ProteoWizard installation folder", "", "",
                                         "msconvert.exe file |msconvert.exe", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if open_file_dialog.ShowModal() == wx.ID_CANCEL:
            return
        else:
            msconvert_path = open_file_dialog.GetPath()
            text_field.SetValue(msconvert_path)
            with open(self.PARAMS_PATH, 'w') as f:
                f.write(msconvert_path)

    def get_msconvert_path(self):
        # if this is not the first time:
        if os.path.exists(self.PARAMS_PATH):
            with open(self.PARAMS_PATH, 'r') as f:
                msconvert_path = f.read()
                return msconvert_path
        else:
            return ''

    def on_help_clicked(self, event):
        webbrowser.open(self.HELP_URL)

    @staticmethod
    def _is_int(s):
        try:
            int(s)
            return True
        except ValueError:
            return False

    @staticmethod
    def _is_float(s):
        try:
            float(s)
            return True
        except ValueError:
            return False

    def _check_input(self, input_dir, output_dir, num_processes, masses):
        # check empty values:
        if input_dir.strip() == '' or output_dir.strip() == '':
            msgBox = wx.MessageDialog(self, "Some of the fields are empty, please fill them all.", caption="Empty fields Error", style=wx.OK|wx.STAY_ON_TOP|wx.ICON_ERROR)
            msgBox.ShowModal()
            return False
        # check numbers:
        if not self._is_int(num_processes):
            msgBox = wx.MessageDialog(self, "Num Processes needs to be a positive integer number.",
                                      caption="Integer field Error", style=wx.OK | wx.STAY_ON_TOP | wx.ICON_ERROR)
            msgBox.ShowModal()
            return False
        # check masses:
        for mass in masses:
            if not self._is_float(mass):
                msgBox = wx.MessageDialog(self, "The masses need to be decimal numbers.",
                                          caption="Float Mass Error", style=wx.OK | wx.STAY_ON_TOP | wx.ICON_ERROR)
                msgBox.ShowModal()
                return False
        return True

    def _run_qc_process(self, **kwargs):
        manager = multiprocessing.Manager()
        self.queue = manager.Queue()
        kwargs['queue'] = self.queue
        self.p = multiprocessing.Process(target=run_qc_process, kwargs=kwargs)
        # self.p.daemon = True
        self.p.start()

        while True:
            # get logger message:
            line = self.queue.get()
            if line == END_PROCESS:
                # print('break')
                break
            else:
                self.logger.AppendText(line)

        # kill orphaned processes:
        # print('run process thread, before terminating p ended: ' + str(os.getpid()))
        parent = psutil.Process(os.getpid())
        children_to_kill = [child for child in parent.children(recursive=True) if child.pid not in self.gui_children_pids]
        children_to_kill_pids = [child.pid for child in children_to_kill]
        # print('children_to_kill: ' + str(children_to_kill_pids))
        for child in children_to_kill:
            try:
                child.kill()  # for child in children_to_kill]
            except psutil.NoSuchProcess:
                pass

        children_left_pids = [child.pid for child in parent.children(recursive=True) if child.pid not in self.gui_children_pids]
        # print('children_left_pids: ' + str(children_left_pids))

        # print('before terminating')
        self.p.terminate()
        # print('between p term and join')
        self.p.join()
        # print('after terminating')

        self.p = None
        self.queue = None
        self.running = False
        if not self.on_exit:
            self.run_qc_button.SetLabel("Run QC")

    def on_close(self, event):
        # print('on close, before: ' + str(os.getpid()))
        if self.p is not None:
            self.on_exit = True
            self.queue.put(END_PROCESS)
        self.Destroy()

    def get_masses_list(self):
        raw_masses = self.masses_list.GetValue().strip().split('\n')
        # print(raw_masses)
        # remove white spaces:
        masses = [raw_mass.strip() for raw_mass in raw_masses]
        # remove blank values:
        masses = [mass for mass in masses if mass]
        # print(masses)
        return masses

    def run_qc_on_click(self, event):
        if not self.running:
            input_dir = self.input_dir.GetPath()
            output_dir = self.output_dir.GetPath()
            num_processes = self.num_processes_text_box.GetValue()
            batch = self.batch_check_box.GetValue()
            no_report = self.no_report_check_box.GetValue()
            masses = self.get_masses_list()
            logger.info('input_dir:' + input_dir)

            # checking input:
            if not self._check_input(input_dir, output_dir, num_processes, masses):
                return
            # now, let run protqc:
            qc_kwargs = {'input': [input_dir], 'output_dir': output_dir, 'num_processes': int(num_processes),
                       'batch': batch, 'no_report': no_report, 'msconvert_path': self.msconvert_exe.GetValue(), 'masses': [float(mass) for mass in masses]}
            self.running = True
            self.run_qc_button.SetLabel("Stop")
            self.qc_thread = Thread(target=self._run_qc_process, kwargs=qc_kwargs)
            # self.qc_thread.daemon = True
            self.qc_thread.start()
        else:
            self.queue.put(END_PROCESS)


def kill_proc_tree(pid, including_parent=True):
    parent = psutil.Process(pid)
    children = parent.children(recursive=True)
    for child in children:
        child.kill()
    gone, still_alive = psutil.wait_procs(children, timeout=5)
    if including_parent:
        parent.kill()
        parent.wait(5)


def stop_all_processes(including_parent):
    me = os.getpid()
    kill_proc_tree(me, including_parent=including_parent)


def main():
    app = wx.App()
    QCReporterGUI(None, 'Raw Beans')
    app.MainLoop()

p_name = multiprocessing.current_process().name
# print(p_name)
# print(__name__)

if __name__ == '__main__':
    # On Windows calling this function(freeze_support) is necessary.
    # On Linux/OSX it does nothing.
    multiprocessing.freeze_support()
    main()
