#!/usr/bin/env python
import argparse
import os
import sys
import csv

from protqc.modules import SearchUtils

from protqc.graph_utils import GraphUtils

from protqc.readers import ThermoSpecReader


DEFAULT_NUM_PROCESSES = 1


def parse_args():
    parser = argparse.ArgumentParser(description="Gets the scan number and ratios of the precursor")
    parser.add_argument('--input', required=True, type=str, help='raw file or folder with raw files')
    parser.add_argument('--output-dir', type=str, required=True, help='csv output')
    return parser.parse_args()


KAPPA_MZ_LIST = [215.1026, 229.1183, 243.1339, 314.171, 328.1867, 342.2023, 357.1769, 427.2551, 441.2708, 445.2115, 455.2864, 459.2272, 473.2428, 488.2173, 528.3028, 542.3184, 546.2592, 556.3341, 560.2749, 574.2905, 589.265, 656.3614, 670.377, 674.3178, 684.3927, 688.3334, 702.3491, 717.3236, 743.3934, 757.409, 761.3498, 771.4247, 775.3655, 789.3811, 804.3556, 840.4462, 854.4618, 858.4026, 868.4775, 872.4182, 886.4339, 901.4084, 925.4989, 939.5146, 943.4553, 953.5302, 957.471, 967.5459, 968.5047, 969.4888, 971.4866, 985.5023, 986.4612, 987.4452, 988.4404, 1026.5466, 1040.5623, 1044.503, 1054.5779, 1055.5368, 1056.5208, 1058.5187, 1069.5524, 1070.5364, 1072.5343, 1073.4932, 1074.4772, 1075.4725, 1087.5088, 1088.4929, 1089.4881, 1139.6307, 1153.6463, 1157.5871, 1167.662, 1168.6208, 1169.6048, 1171.6027, 1174.5409, 1182.6365, 1183.6205, 1185.6184, 1186.5773, 1187.5613, 1188.5565, 1200.5929, 1201.5769, 1202.5722]
LAMBDA_MZ_LIST = [199.0714, 263.1026, 270.1085, 298.1398, 383.1926, 394.1431, 411.2239, 484.2402, 507.2272, 512.2715, 608.2749, 612.2988, 640.3301, 709.3516, 736.3334, 737.3829, 780.3887, 806.4043, 833.3862, 834.4356, 865.4527, 867.4207, 893.4364, 921.4677, 938.4578, 952.4847, 964.4735, 966.4891, 970.4451, 992.5048, 1020.5361, 1023.5218, 1051.5531, 1057.4771, 1156.5456]


PLOT_WIDTH = 12.1
PLOT_HEIGHT = 8

MAX_INTENSITY_THRESHOLD = 3000


def main():
    mz_values = list(set(KAPPA_MZ_LIST).union(set(LAMBDA_MZ_LIST)))
    mz_values.sort()
    args = parse_args()
    cmd = ' '.join(sys.argv)
    if os.path.isdir(args.input):
        paths = {os.path.splitext(file)[0]: os.path.join(args.input, file) for file in os.listdir(args.input) if os.path.isfile(os.path.join(args.input, file)) and file.endswith('.raw')}
    else:
        paths = {os.path.splitext(os.path.basename(args.input))[0]: args.input}
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)
    # print(paths)
    # print(len(paths))
    for name in paths:
        print('Analyzing sample %s' % name)
        sample_output_dir = os.path.join(args.output_dir, name)
        if os.path.exists(sample_output_dir):
            print('Folder already exist - Skipping..')
        else:
            spec_reader = None
            try:
                spec_reader = ThermoSpecReader(paths[name])
            except Exception as e:
                print('Error opening file!')
                print(str(e))
                continue
            for spec in spec_reader:
                if spec.ms_level == 2 and len(spec.mz_array) > 0:
                    print('scan %s' % spec.scan_number)
                    current_sizes = []
                    for mz_value in mz_values:
                        idx = SearchUtils.binary_search_ppm(spec.mz_array, mz_value, ppm=10)
                        if idx != -1:
                            current_sizes.append(spec.intensity_array[idx])
                        else:
                            current_sizes.append(0)
                    if max(current_sizes) > MAX_INTENSITY_THRESHOLD:
                        print('printing graph...')
                        kappa_current_sizes = [size if mz_values[i] in KAPPA_MZ_LIST else 0 for (i, size) in enumerate(current_sizes)]
                        lambda_current_sizes = [size if mz_values[i] in LAMBDA_MZ_LIST else 0 for (i, size) in enumerate(current_sizes)]
                        # print('current_sizes: %s' % current_sizes)
                        if not os.path.exists(sample_output_dir):
                            os.makedirs(sample_output_dir)
                        GraphUtils.bar_graph(os.path.join(sample_output_dir, 'scan-%s.png' % spec.scan_number), 'Scan-%s' % spec.scan_number, 'mz value', mz_values, 'intensity',
                                             [kappa_current_sizes, lambda_current_sizes], traces=['kappa', 'lambda'], spacing=10, figsize=(PLOT_WIDTH,PLOT_HEIGHT))


if __name__ == '__main__':
    main()
