#!/usr/bin/env python
import argparse
from pyteomics import mzml


def parse_args():
    parser = argparse.ArgumentParser(description="Prints specific mzML spectrums")
    parser.add_argument('--input', type=str, help='mzML file.')
    parser.add_argument('--spec-ids', nargs='+', type=str, required=True, help='Output folder, were the report will be written.')
    return parser.parse_args()


def print_specs(mzml_path, spec_ids):
    with mzml.MzML(mzml_path) as reader:
        for spec_id in spec_ids:
            spectrum = reader[spec_id]
            print('spectrum: %s' % spec_id)
            for (key, value) in spectrum.items():
                print('%s: %s' % (key, value))
            print('---------------------------')


def main():
    args = parse_args()
    print_specs(args.input, args.spec_ids)


if __name__ == '__main__':
    main()
