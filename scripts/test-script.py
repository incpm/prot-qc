from bisect import bisect_left, bisect_right


def get_binary_range(a, from_value, to_value):
    lo = 0
    hi = len(a)
    pos_from = bisect_left(a, from_value, lo, hi)
    pos_to = bisect_right(a, to_value, lo, hi)
    return pos_from, pos_to


def binary_search_closest(a, x):  # can't use a to specify default for hi
    lo = 0
    hi = len(a)  # hi defaults to len(a)
    if hi == 0:
        return -1
    pos = bisect_left(a, x, lo, hi)  # find insertion position
    if pos != hi and a[pos] == x:
        return pos
    elif pos == hi:
        pos = pos - 1
    elif pos != 0:
        if a[pos] - x > x - a[pos - 1]:
            pos = pos - 1
    return pos


def binary_search_ppm(a, x):
    pos = binary_search_closest(a, x)
    if pos != -1 and is_ppm_equal(x, a[pos]):
        return pos
    else:
        return -1


def is_ppm_equal(m1, m2, ppm=10):
    calc = abs(((m1 - m2) / m1) * 1000000)
    print(calc)
    return calc <= ppm


# a = [1.03, 1.034, 1.034, 1.038, 1.041, 1.041, 1.041, 1.041, 1.044, 1.044, 1.044, 1.049]
#
# print(get_binary_range(a, 1.031, 1.043))


# print(binary_search([1,1,1,1,1,1], 1))





# from threading import Thread
# from time import sleep
# import multiprocessing
#
#
# END_PROCESS = 'END!'
# queue = None
#
#
# def p1(queue, a, b):
#     print('Hi from process: %s, %s' % (a, b))
#     queue.put('bla1')
#     sleep(1)
#     queue.put('bla2')
#     sleep(1)
#     queue.put('bla3')
#     sleep(1)
#     queue.put('bla4')
#     sleep(15)
#
#
# def t1(**kwargs):
#     print('Hi form thread')
#     manager = multiprocessing.Manager()
#     global queue
#     queue = manager.Queue()
#     kwargs['queue'] = queue
#     p = multiprocessing.Process(target=p1, kwargs=kwargs)
#     p.start()
#     while True:
#         msg = queue.get()
#         if msg == END_PROCESS:
#             print('break')
#             break
#         else:
#             print(msg)
#     p.terminate()
#     p.join()
#
#
# def run():
#     qc_thread = Thread(target=t1, kwargs={'a':1, 'b': 2})
#     qc_thread.start()
#     sleep(3)
#     queue.put(END_PROCESS)
#     qc_thread.join()
#     print('main')
#
# if __name__ == '__main__':
#     run()
#
#
#
#
#






# import dask
# from dask.delayed import delayed
# from dask.multiprocessing import get
# from dask.threaded import get
# from time import sleep
#
#
# def inc(x):
#     return x + 1
#
#
# def add(x, y):
#     return x + y
#
#
# def func(a):
#     print('sleeping for 5 sec')
#     sleep(5)
#     for i in range(100):
#         print(a)
#     return a
#
# l = []
# for i in range(4):
#     l.append(dask.delayed(func)(i))
# # dask.multiprocessing.get
# # dask.threaded.get
# l = dask.compute(l, get=dask.multiprocessing.get)
# print(l)

# x = dask.delayed(inc)(1)
# y = dask.delayed(inc)(2)
# z = dask.delayed(add)(x, y)
# print(z.compute())
# z.vizualize()
# dot_graph(z.dask)
#
#
# import dask.array as da
# x = da.ones((5, 15), chunks=(5, 5))
# x.dask
# d = (x + 1).dask
# dot_graph(d)

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import seaborn as sns
plt.style.use('seaborn')
import numpy as np

PLOT_WIDTH = 7.1875
PLOT_HEIGHT = 4.6875
COLORS = cm.rainbow(np.linspace(0, 1, 10))
MARKER_SIZE = 10
ALPHA = 0.8


def scatter_graph(file_path, title, x_title, x, y_title, y, traces=None, axis_range=None, density=False):
    plt.figure(1, figsize=(PLOT_WIDTH, PLOT_HEIGHT))
    if isinstance(x[0], list):
        for i in range(len(x)):
            plt.scatter(x[i], y[i], marker="o", alpha=ALPHA, s=10, color=COLORS[i], label=traces[i])
            if density:
                sns.kdeplot(x[i], y[i], cmap="Reds", shade=False)
    else:
        plt.scatter(x, y, marker="o", alpha=ALPHA, s=10, color=COLORS[0])
        if density:
            sns.kdeplot(x, y, cmap="Reds", shade=False)
    plt.legend()
    plt.title(title)
    plt.xlabel(x_title)
    plt.ylabel(y_title)
    if axis_range:
        plt.xlim(axis_range['x'])
        plt.ylim(axis_range['y'])
    plt.savefig(file_path)
    plt.clf()

def line_graph(file_path, title, x_title, x, y_title, y, traces=None, axis_range=None):
    plt.figure(1, figsize=(PLOT_WIDTH, PLOT_HEIGHT))
    if isinstance(x[0], list):
        for i in range(len(x)):
            plt.plot(x[i], y[i], marker="o", alpha=ALPHA, markersize=10, color=COLORS[i], label=traces[i])
    else:
        plt.plot(x, y, marker="o", alpha=ALPHA, markersize=10, color=COLORS[0])
    plt.legend()
    plt.title(title)
    plt.xlabel(x_title)
    plt.ylabel(y_title)
    if axis_range:
        plt.xlim(axis_range['x'])
        plt.ylim(axis_range['y'])
    plt.savefig(file_path)
    plt.clf()

def hist_graph(file_path, title, x_title, x, y_title, traces=None, axis_range=None):
    plt.figure(1, figsize=(PLOT_WIDTH, PLOT_HEIGHT))
    if isinstance(x[0], list):
        plt.hist(x, color=COLORS[:len(x)], label=traces[:len(x)])
    else:
        plt.hist(x, color=COLORS[0])
    plt.legend()
    plt.title(title)
    plt.xlabel(x_title)
    plt.ylabel(y_title)
    if axis_range:
        plt.xlim(axis_range['x'])
        plt.ylim(axis_range['y'])
    plt.savefig(file_path)
    plt.clf()

def bar_graph(file_path, title, x_title, x_labels, y_title, sizes, traces=None, axis_range=None, rotation=None):
    plt.figure(1, figsize=(PLOT_WIDTH, PLOT_HEIGHT))
    x = list(range(len(x_labels)))
    if isinstance(sizes[0], list):
        width = 0.8/len(sizes)
        start_pos = -0.3
        x = np.array(x)
        for i in range(len(sizes)):
            plt.bar(x + start_pos + (width * i), height=sizes[i], width=width, color=COLORS[i], label=traces[i])
    else:
        plt.bar(x, height=sizes, color=COLORS[0])
    plt.gca().set_xticks(x)
    plt.gca().set_xticklabels(x_labels)
    if rotation is not None:
        plt.xticks(rotation=rotation)
    plt.legend()
    plt.title(title)
    plt.xlabel(x_title)
    plt.ylabel(y_title)
    if axis_range:
        plt.xlim(axis_range['x'])
        plt.ylim(axis_range['y'])
    plt.savefig(file_path)
    plt.clf()

def pie_graph(file_path, title, labels, sizes):
    plt.figure(1, figsize=(PLOT_WIDTH, PLOT_HEIGHT))
    plt.pie(sizes, labels=labels, autopct='%1.2f%%',)
    plt.gca().axis('equal')
    sum_sizes = sum(sizes)
    plt.legend(labels=['%s - %1.2f %%' % (l, s/sum_sizes*100) for l, s in zip(labels, sizes)])
    plt.title(title)
    plt.savefig(file_path)
    plt.clf()



# plot_bar('bar plot!', 'x axis', [], 'y axis', [], [], range=None)

def test():
    x = [1,2,3,4,5,6,7]
    y = np.array([1,2,3,4,5,6,7])
    y2 = y + 3
    y3 = y ** 2
    y4 = y3 + 3
    y5 = y3 + 7
    # scatter_graph('graphs-tests/scatter-graph.png', 'scatter test', 'x axis', [x, x, x], 'y axis', [y3, y4, y5], traces=['sample1', 'sample2', 'sample3'], axis_range=None, density=False)
    # line_graph('graphs-tests/line-graph.png', 'line test', 'x axis', [x, x, x], 'y axis', [y3, y4, y5], traces=['sample1', 'sample2', 'sample3'], axis_range=None)
    # pie_graph('graphs-tests/pie-chart.png', 'pie chart', ['a', 'b', 'c', 'd', 'e'], [35,0,10,50,100],)
    bar_graph('graphs-tests/bar-graph.png', 'bar-graph', 'a_axis', [2,3,4,5,6], 'y_axis', [[35,0,56,50,100], [45,0,10,30,100], [35,0,10,50,30]], traces=['sample1', 'sample2', 'sample3'], axis_range=None)
    # hist_graph('graphs-tests/hist-graph.png', 'hist-graph', 'a_axis', [[1]*10+[2]*2+[3]*30+[4]*50+[5]*1+[6]*100], 'y_axis', traces=['sample1'], axis_range=None)
    # print(matplotlib.rcParams['hist.bins'])


# test()




plt.figure(1, figsize=(PLOT_WIDTH, PLOT_HEIGHT))
plt.suptitle("Main Title", size=16, y=1.05)
ax = plt.subplot(141)
plt.scatter([1,1.3,-1,-0.3,0,0.7, -0.2, 0.3, 1.2, -1.5], [1,2,3,4,5,6,7,8,9,10], marker="o", color=COLORS[0])
plt.text(1, 1, "title1 is the best title", verticalalignment='top',
         rotation=270,
         fontsize=20, transform=ax.transAxes)
plt.subplot(142)
plt.scatter([1,1.3,-1,-0.3,0,0.7, -0.2, 0.3, 1.2, -1.5], [1,2,3,4,5,6,7,8,9,10], marker="o", color=COLORS[0])
plt.legend()
plt.title('Title', y=1.04)
plt.xlabel('Dev')
plt.ylabel('Ret')
plt.tight_layout()
plt.savefig('graphs-tests/mass-test.png')
plt.clf()