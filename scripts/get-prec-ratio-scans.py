#!/usr/bin/env python
import argparse
import os
import sys
import csv

from protqc.analyzer import QCAnalyzer
from protqc.runner import QCRunner
from protqc.modules import MassDeviationComponent, PREC_RATIO, MS2PrecRatioComponent

DEFAULT_NUM_PROCESSES = 1


def parse_args():
    parser = argparse.ArgumentParser(description="Gets the scan number and ratios of the precursor")
    parser.add_argument('--input', required=True, type=str, help='mzml file')
    parser.add_argument('--output-dir', type=str, required=True, help='csv output')
    parser.add_argument('--cores', type=int, default=DEFAULT_NUM_PROCESSES, help='Number of processes to use, Default: %d' % DEFAULT_NUM_PROCESSES)
    return parser.parse_args()


def main():
    args = parse_args()
    cmd = ' '.join(sys.argv)
    print('Analyzing..')
    qc_analyzer = QCAnalyzer(args.input)
    qc_data = qc_analyzer.analyze(num_workers=args.cores, components_dict={PREC_RATIO: MS2PrecRatioComponent})
    print('Writing output..')
    for sample in qc_data.samples_dict:
        prec_ratio_component = qc_data.samples_dict[sample].components_dict[PREC_RATIO]
        with open(os.path.join(args.output_dir, '%s_prec_ratio.csv' % sample), 'w') as out_fd:
            csv_writer = csv.writer(out_fd)
            csv_writer.writerow(['scan_number', 'prec_ratio'])
            for i in range(len(prec_ratio_component.scan_numbers)):
                csv_writer.writerow([prec_ratio_component.scan_numbers[i], prec_ratio_component.ms2_prec_ratio[i]])


if __name__ == '__main__':
    main()
