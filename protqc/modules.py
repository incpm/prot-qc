import functools
import os
from collections import Counter, deque
from typing import List

import numpy as np
import pandas as pd
from bisect import bisect_left, bisect_right
from abc import ABC, abstractmethod
import traceback

from protqc.settings import IMAGES, RESOURCES
from protqc.json_utils import JSONable
from protqc.graph_utils import GraphUtils

# logging
import logging

logger = logging.getLogger(__name__)


class Utils(object):
    @staticmethod
    def list_chunks(l, n):
        return [l[i:i + n] for i in range(0, len(l), n)]

    @staticmethod
    def calc_range_sizes(values, bounds):
        values = sorted(values)
        sizes = [0] * (len(bounds) + 1)
        current_bound_idx = 0
        for (i, value) in enumerate(values):
            while current_bound_idx < len(bounds) and value > bounds[current_bound_idx]:
                current_bound_idx += 1
            # case of last pie slice:
            if current_bound_idx == len(bounds):
                sizes[current_bound_idx] = len(values) - i
                break
            sizes[current_bound_idx] += 1
        return sizes

    @staticmethod
    def get_filtered_xy_on_x(a, b, value):
        filtered = [[], []]
        non_filtered = [[], []]
        if value is not None:
            for x in zip(a, b):
                if x[0] == value:
                    filtered[0].append(x[0])
                    filtered[1].append(x[1])
                else:
                    non_filtered[0].append(x[0])
                    non_filtered[1].append(x[1])
        return filtered, non_filtered


# some utils for array performance:
class SearchUtils(object):
    @staticmethod
    def binary_search(a, x, lo=0, hi=None):  # can't use a to specify default for hi
        hi = hi if hi is not None else len(a)  # hi defaults to len(a)
        pos = bisect_left(a, x, lo, hi)  # find insertion position
        return pos if pos != hi and a[pos] == x else -1  # don't walk off the end

    @staticmethod
    def get_binary_search_range(a, from_value, to_value):
        lo = 0
        hi = len(a)
        pos_from = bisect_left(a, from_value, lo, hi)
        pos_to = bisect_right(a, to_value, lo, hi)
        return pos_from, pos_to

    @staticmethod
    def binary_search_closest(a, x):
        lo = 0
        hi = len(a)
        if hi == 0:
            return -1
        pos = bisect_left(a, x, lo, hi)
        if pos != hi and a[pos] == x:
            return pos
        elif pos == hi:
            pos = pos - 1
        elif pos != 0:
            if a[pos] - x > x - a[pos - 1]:
                pos = pos - 1
        return pos

    @staticmethod
    def binary_search_ppm(a, x, ppm=10):
        pos = SearchUtils.binary_search_closest(a, x)
        if pos != -1 and SearchUtils.is_ppm_equal(x, a[pos], ppm=ppm):
            return pos
        else:
            return -1

    @staticmethod
    def is_ppm_equal(m1, m2, ppm=10):
        abs_ppm_diff = abs(SearchUtils.get_ppm_diff(m1, m2))
        return abs_ppm_diff <= ppm

    @staticmethod
    def get_ppm_diff(m1, m2):
        calc = ((m1 - m2) / m1) * 1000000
        return calc


class Resourceable(ABC):
    @abstractmethod
    def prep_resources(self, output_dir):
        pass

    @abstractmethod
    def generate_resources(self):
        pass


class QCComponent(JSONable):
    KEEP_PARAMS = ['sample']

    def __init__(self, sample, keep_params=None):
        self.sample = sample
        keep_params = keep_params + QCComponent.KEEP_PARAMS if keep_params else QCComponent.KEEP_PARAMS
        # logger.info('keep params: %s' % keep_params)
        JSONable.__init__(self, keep_params=keep_params)

    @abstractmethod
    def read_spec(self, spec):
        pass

    @abstractmethod
    def finish(self):
        pass


class QCResourceComponent(QCComponent, Resourceable):
    def __init__(self, sample, keep_params=None):
        Resourceable.__init__(self)
        QCComponent.__init__(self, sample=sample, keep_params=keep_params)

    def prep_resources(self, output_dir):
        self._arrange_plot_data()
        self._set_resources_paths(output_dir)

    @abstractmethod
    def _arrange_plot_data(self):
        pass

    @abstractmethod
    def _set_resources_paths(self, output_dir):
        pass

    @abstractmethod
    def generate_resources(self, ranges_dict=None):
        pass


class QCModule(JSONable):
    KEEP_PARAMS = ['samples_dict', 'samples']

    def __init__(self, samples, samples_dict, keep_params=None):
        keep_params = keep_params + QCModule.KEEP_PARAMS if keep_params else QCModule.KEEP_PARAMS
        JSONable.__init__(self, keep_params=keep_params)
        self.samples_dict = samples_dict
        self.samples = samples


class QCResourceModule(QCModule, Resourceable):
    SAMPLES_CHUNK_SIZE = 10

    def __init__(self, sample, samples_dict, keep_params=None):
        Resourceable.__init__(self)
        QCModule.__init__(self, sample, samples_dict, keep_params)

    def _get_range(self, field, padding_min=0.15, padding_max=0.15, min_value=None, max_value=None):
        """
        Calculates range of data in all samples.
        :param field: can be either a list of arrays (for each sample), or the field name in the SampleQCSData object.
        :param padding_min: the percentage of min padding
        :param padding_max: the percentage of max padding
        :param min_value: min value of the range
        :param max_value: max value of the range
        :return: the
        """
        range = None
        try:
            if type(field) is str:
                min_value = min([min(self.samples_dict[sample].__dict__[field]) for sample in
                                 self.samples]) if min_value is None else min_value
                max_value = max([max(self.samples_dict[sample].__dict__[field]) for sample in
                                 self.samples]) if max_value is None else max_value
            else:
                min_value = min([min(arr) for arr in field]) if min_value is None else min_value
                max_value = max([max(arr) for arr in field]) if max_value is None else max_value
            padding_min_value = padding_min * (max_value - min_value)
            padding_max_value = padding_max * (max_value - min_value)
            range = (min_value - padding_min_value, max_value + padding_max_value)
        except Exception:
            id = '%s.%s' % (type(self).__name__, field) if type(field) is str else type(self).__name__
            logger.warning(
                '%s field raised an exception while calculating graph range, it must be empty in at least one sample. trying to continue...' % id)
            # logger.warning(traceback.format_exc())
        return range

    def _chunk_field(self, field_name):
        all_samples_data = [self.samples_dict[sample].__dict__[field_name] for sample in self.samples]
        return Utils.list_chunks(all_samples_data, self.SAMPLES_CHUNK_SIZE)

    def prep_resources(self, output_dir):
        # first prep resources for childs, so it can you their data after that.
        for sample in self.samples:
            self.samples_dict[sample].prep_resources(output_dir)
        self.sample_chunks = Utils.list_chunks(self.samples, self.SAMPLES_CHUNK_SIZE)
        self._unite_samples_data()
        ranges_dict = self._calc_ranges()
        self._set_resources_paths(output_dir)
        return ranges_dict

    @abstractmethod
    def _calc_ranges(self):
        pass

    @abstractmethod
    def _unite_samples_data(self):
        pass

    @abstractmethod
    def _set_resources_paths(self, output_dir):
        pass

    @abstractmethod
    def generate_resources(self, recursive=True):
        pass


class MS2CountsComponent(QCComponent):
    def finish(self):
        pass

    KEEP_PARAMS = ['ms2_count']

    def __init__(self, sample, ms2_count=None):
        QCComponent.__init__(self, sample, keep_params=MS2CountsComponent.KEEP_PARAMS)
        self.ms2_count = ms2_count if ms2_count is not None else 0

    def read_spec(self, spec):
        if spec.ms_level == 2:
            self.ms2_count += 1


class TopNComponent(QCResourceComponent):
    KEEP_PARAMS = ['top_n']

    def __init__(self, sample, top_n=None):
        super().__init__(sample, self.KEEP_PARAMS)
        self.top_n_count = 0
        self.top_n = top_n if top_n is not None else []
        self.ms1_count = 0

    def read_spec(self, spec):
        if spec.ms_level == 1:
            if self.ms1_count > 0:
                self.top_n.append(self.top_n_count)
            self.ms1_count += 1
            self.top_n_count = 0
        elif spec.ms_level == 2:
            self.top_n_count += 1

    def finish(self):
        self.top_n.append(self.top_n_count)

    def _arrange_plot_data(self):
        self.top_n_dict_sizes = dict(Counter(self.top_n))
        top_n_keys = self.top_n_dict_sizes.keys()
        max_top_n_keys = 0
        try:
            max_top_n_keys = max(top_n_keys)
        except Exception:
            logger.warning('No top n for sample: %s' % self.sample)
        self.top_n_labels = list(range(min(top_n_keys), max_top_n_keys + 1))
        self.top_n_sizes = [self.top_n_dict_sizes[label] if label in self.top_n_dict_sizes else 0 for label in
                            self.top_n_labels]
        self.top_n_log_sizes = list(np.log10(np.array(self.top_n_sizes) + 1))

    def _set_resources_paths(self, output_dir):
        self.top_n_relative_path = os.path.join(IMAGES, '%s-top-n.png' % self.sample)
        self.top_n_full_path = os.path.join(output_dir, self.top_n_relative_path)

    def generate_resources(self, ranges_dict=None):
        top_n_sapcing = ((int(ranges_dict['x'][1]) - int(ranges_dict['x'][0])) // 10) if ranges_dict is not None else 1
        GraphUtils.bar_graph(self.top_n_full_path, self.sample, 'TopN', self.top_n_labels, 'Counts (log10)',
                             self.top_n_log_sizes, axis_range=ranges_dict, spacing=top_n_sapcing)


class TopNModule(QCResourceModule):
    def __init__(self, samples, samples_dict):
        super().__init__(samples, samples_dict)

    def _unite_samples_data(self):
        top_n_range = self._get_range('top_n', padding_min=0, padding_max=0)
        top_ns_labels = [list(range(top_n_range[0], top_n_range[1] + 1)) for sample in self.samples]
        self.top_ns_labels_chunks = Utils.list_chunks(top_ns_labels, self.SAMPLES_CHUNK_SIZE)
        top_ns_log_sizes = [[np.log10(self.samples_dict[sample].top_n_dict_sizes[label] + 1)
                             if label in self.samples_dict[sample].top_n_dict_sizes else 0
                             for label in top_ns_labels[0]] for sample in self.samples]
        self.top_ns_log_sizes_chunks = Utils.list_chunks(top_ns_log_sizes, self.SAMPLES_CHUNK_SIZE)

    def _calc_ranges(self):
        self.top_n_range = self._get_range('top_n', padding_min=0, padding_max=0)
        self.top_n_log_sizes_range = self._get_range('top_n_log_sizes', min_value=0, padding_min=0)
        self.top_n_ranges = {'x': self.top_n_range, 'y': self.top_n_log_sizes_range}
        return self.top_n_ranges

    def _set_resources_paths(self, output_dir):
        self.top_n_relative_paths = []
        self.top_n_full_paths = []
        for (i, samples_chunk) in enumerate(self.sample_chunks):
            self.top_n_relative_paths.append(os.path.join(IMAGES, 'all-samples%s-top-n.png' % (i + 1)))
            self.top_n_full_paths.append(os.path.join(output_dir, self.top_n_relative_paths[i]))

    def generate_resources(self, recursive=True):
        for (i, samples_chunk) in enumerate(self.sample_chunks):
            # top n graphs:
            top_ns_spacing = (int(self.top_n_ranges['x'][1]) - int(self.top_n_ranges['x'][0])) // 10 if \
                self.top_n_ranges['x'] is not None else None
            GraphUtils.line_graph(self.top_n_full_paths[i], 'All Samples%s' % (i + 1), 'TopN',
                                  self.top_ns_labels_chunks[i],
                                  'Counts (log10)', self.top_ns_log_sizes_chunks[i], traces=samples_chunk,
                                  axis_range={'y': self.top_n_log_sizes_range}, spacing=top_ns_spacing)
        if recursive:
            for sample in self.samples:
                self.samples_dict[sample].generate_resources(ranges_dict=self.top_n_ranges)


class ChargeDistComponent(QCResourceComponent):
    def finish(self):
        pass

    KEEP_PARAMS = ['ms2_charge_state']

    # bounds note - each element in the array represents less then and equal, for example [2,4] represents three parts x<=2, 2<x<=4, x<4
    CHARGE_STATE_BOUNDS = [0, 1, 2, 3, 4, 5, 6, 7]
    CHARGE_STATE_LABELS = ['0', '1', '2', '3', '4', '5', '6', '7', '8']

    def __init__(self, sample, ms2_charge_state=None):
        super().__init__(sample, self.KEEP_PARAMS)
        self.ms2_charge_state = ms2_charge_state if ms2_charge_state is not None else []

    def read_spec(self, spec):
        if spec.ms_level == 2:
            if spec.ms2_charge_state is not None:
                self.ms2_charge_state.append(spec.ms2_charge_state)

    def _arrange_plot_data(self):
        self.ms2_charge_state_labels = ChargeDistComponent.CHARGE_STATE_LABELS
        self.ms2_charge_state_sizes = Utils.calc_range_sizes(self.ms2_charge_state,
                                                             ChargeDistComponent.CHARGE_STATE_BOUNDS)

    def _set_resources_paths(self, output_dir):
        self.charge_state_relative_path = os.path.join(IMAGES, '%s-charge-state.png' % self.sample)
        self.charge_state_full_path = os.path.join(output_dir, self.charge_state_relative_path)

    def generate_resources(self, ranges_dict=None):
        GraphUtils.bar_graph(self.charge_state_full_path, self.sample, 'Charge Distribution',
                             self.ms2_charge_state_labels, 'Counts (log10)',
                             self.ms2_charge_state_sizes, axis_range=ranges_dict)


class ChargeDistModule(QCResourceModule):
    def __init__(self, samples, samples_dict):
        super().__init__(samples, samples_dict)

    def _unite_samples_data(self):
        self.ms2_charge_states_labels_chunks = self._chunk_field('ms2_charge_state_labels')
        self.ms2_charge_states_sizes_chunks = self._chunk_field('ms2_charge_state_sizes')

    def _calc_ranges(self):
        self.ms2_charge_state_sizes_range = self._get_range('ms2_charge_state_sizes', min_value=0, padding_min=0)
        self.charge_dist_ranges = {'y': self.ms2_charge_state_sizes_range}
        return self.charge_dist_ranges

    def _set_resources_paths(self, output_dir):
        self.charge_state_relative_paths = []
        self.charge_state_full_paths = []
        for (i, samples_chunk) in enumerate(self.sample_chunks):
            self.charge_state_relative_paths.append(os.path.join(IMAGES, 'all-samples%s-charge-state.png' % (i + 1)))
            self.charge_state_full_paths.append(os.path.join(output_dir, self.charge_state_relative_paths[i]))

    def generate_resources(self, recursive=True):
        for (i, samples_chunk) in enumerate(self.sample_chunks):
            # charge state graphs:
            GraphUtils.bar_graph(self.charge_state_full_paths[i], 'All Samples%s' % (i + 1), 'Charge Distribution',
                                 self.ms2_charge_states_labels_chunks[i][0], 'Counts (log10)',
                                 self.ms2_charge_states_sizes_chunks[i], traces=samples_chunk,
                                 axis_range=self.charge_dist_ranges)
        if recursive:
            for sample in self.samples:
                self.samples_dict[sample].generate_resources(ranges_dict=self.charge_dist_ranges)


class MS2InjectTimeComponent(QCResourceComponent):
    def finish(self):
        pass

    KEEP_PARAMS = ['ms2_inject']

    # bounds note - each element in the array represents less then and equal, for example [2,4] represents three parts x<=2, 2<x<=4, x>4
    MS2_INJECT_BOOUNDS = list(range(0, 150, 5))
    MS2_INJECT_LABELS = MS2_INJECT_BOOUNDS + [150]

    def __init__(self, sample, ms2_inject=None):
        super().__init__(sample, keep_params=self.KEEP_PARAMS)
        self.ms2_inject = ms2_inject if ms2_inject is not None else []

    def read_spec(self, spec):
        if spec.ms_level == 2 and spec.injection_time is not None:
            self.ms2_inject.append(spec.injection_time)

    def _arrange_plot_data(self):
        max_inject = 0
        try:
            max_inject = round(max(self.ms2_inject))
        except Exception:
            logger.warning('No injection time for sample: %s' % self.sample)
        bounds = list(range(0, max_inject, 5))
        self.ms2_inject_labels = bounds + [max_inject]
        self.ms2_inject_sizes = Utils.calc_range_sizes(self.ms2_inject, bounds)
        self.ms2_inject_log_sizes = list(np.log10(np.array(self.ms2_inject_sizes) + 1))

    def _set_resources_paths(self, output_dir):
        self.ms2_inject_relative_path = os.path.join(IMAGES, '%s-ms2-inject.png' % self.sample)
        self.ms2_inject_full_path = os.path.join(output_dir, self.ms2_inject_relative_path)

    def generate_resources(self, ranges_dict=None):
        GraphUtils.bar_graph(self.ms2_inject_full_path, self.sample, 'Injection Time (msec)',
                             self.ms2_inject_labels, 'Counts (log10)', self.ms2_inject_log_sizes,
                             axis_range=ranges_dict, spacing=3)


class MS2InjectTimeModule(QCResourceModule):
    def __init__(self, samples, samples_dict):
        super().__init__(samples, samples_dict)

    def _unite_samples_data(self):
        self.ms2_injects_labels_chunks = self._chunk_field('ms2_inject_labels')
        self.ms2_injects_log_sizes_chunks = self._chunk_field('ms2_inject_log_sizes')

    def _calc_ranges(self):
        self.ms2_inject_log_sizes_range = self._get_range('ms2_inject_log_sizes', min_value=0, padding_min=0)
        self.ms2_inject_ranges = {'y': self.ms2_inject_log_sizes_range}
        return self.ms2_inject_ranges

    def _set_resources_paths(self, output_dir):
        self.ms2_inject_relative_paths = []
        self.ms2_inject_full_paths = []
        for (i, samples_chunk) in enumerate(self.sample_chunks):
            self.ms2_inject_relative_paths.append(os.path.join(IMAGES, 'all-samples%s-ms2-inject.png' % (i + 1)))
            self.ms2_inject_full_paths.append(os.path.join(output_dir, self.ms2_inject_relative_paths[i]))

    def generate_resources(self, recursive=True):
        for (i, samples_chunk) in enumerate(self.sample_chunks):
            # ms2 inject graphs:
            GraphUtils.line_graph(self.ms2_inject_full_paths[i], 'All Samples%s' % (i + 1), 'Injection Time (msec)',
                                  self.ms2_injects_labels_chunks[i], 'Counts (log10)',
                                  self.ms2_injects_log_sizes_chunks[i],
                                  traces=samples_chunk, axis_range=self.ms2_inject_ranges, legend_loc=2, spacing=3)
        if recursive:
            for sample in self.samples:
                self.samples_dict[sample].generate_resources(ranges_dict=self.ms2_inject_ranges)


class RetVsTopNComponent(QCResourceComponent):
    KEEP_PARAMS = ['ms1_ret', 'top_n']

    def __init__(self, sample, ms1_ret=None, top_n=None):
        super().__init__(sample, keep_params=self.KEEP_PARAMS)
        self.ms1_ret = ms1_ret if ms1_ret is not None else []
        self.top_n = top_n if top_n is not None else []
        self.top_n_count = 0
        self.ms1_count = 0

    def read_spec(self, spec):
        if spec.ms_level == 1:
            if self.ms1_count > 0:
                self.top_n.append(self.top_n_count)
            self.ms1_count += 1
            self.top_n_count = 0
            self.ms1_ret.append(spec.scan_start_time)
        elif spec.ms_level == 2:
            self.top_n_count += 1

    def finish(self):
        self.top_n.append(self.top_n_count)

    def _arrange_plot_data(self):
        pass

    def _set_resources_paths(self, output_dir):
        self.ms1_ret_vs_top_n_relative_path = os.path.join(IMAGES, '%s-ms1-ret-vs-top-n.png' % self.sample)
        self.ms1_ret_vs_top_n_full_path = os.path.join(output_dir, self.ms1_ret_vs_top_n_relative_path)

    def generate_resources(self, ranges_dict=None):
        zero, non_zero = Utils.get_filtered_xy_on_x(self.top_n, self.ms1_ret, 0)
        GraphUtils.scatter_graph(self.ms1_ret_vs_top_n_full_path, self.sample, 'Retention Time (min)',
                                 [non_zero[1], zero[1]], 'TopN', [non_zero[0], zero[0]], traces=['',''], axis_range=ranges_dict)


class RetVsTopNModule(QCResourceModule):
    def __init__(self, samples, samples_dict):
        super().__init__(samples, samples_dict)

    def _unite_samples_data(self):
        pass

    def _calc_ranges(self):
        self.top_n_range = self._get_range('top_n', padding_min=0, padding_max=0)
        self.ms1_ret_range = self._get_range('ms1_ret')
        self.ret_vs_top_n_ranges = {'x': self.ms1_ret_range, 'y': self.top_n_range}
        return self.ret_vs_top_n_ranges

    def _set_resources_paths(self, output_dir):
        pass

    def generate_resources(self, recursive=True):
        if recursive:
            for sample in self.samples:
                self.samples_dict[sample].generate_resources(ranges_dict=self.ret_vs_top_n_ranges)


class InjectVsRetComponent(QCResourceComponent):
    def finish(self):
        pass

    KEEP_PARAMS = ['ms1_inject', 'ms1_ret']

    def __init__(self, sample, ms1_inject=None, ms1_ret=None):
        super().__init__(sample, keep_params=self.KEEP_PARAMS)
        self.ms1_inject = ms1_inject if ms1_inject is not None else []
        self.ms1_ret = ms1_ret if ms1_ret is not None else []

    def read_spec(self, spec):
        if spec.ms_level == 1 and spec.injection_time is not None:
            self.ms1_inject.append(spec.injection_time)
            self.ms1_ret.append(spec.scan_start_time)

    def _arrange_plot_data(self):
        pass

    def _set_resources_paths(self, output_dir):
        self.ms1_inject_vs_ret_relative_path = os.path.join(IMAGES, '%s-ms1-inject-vs-ret.png' % self.sample)
        self.ms1_inject_vs_ret_full_path = os.path.join(output_dir, self.ms1_inject_vs_ret_relative_path)

    def generate_resources(self, ranges_dict=None):
        max_inject = None
        try:
            max_inject = max(self.ms1_inject)
        except Exception:
            logger.warning('No injection time for sample: %s' % self.sample)
            # logger.warning(traceback.format_exc())
        maxx, non_maxx = Utils.get_filtered_xy_on_x(self.ms1_inject, self.ms1_ret, max_inject)
        GraphUtils.scatter_graph(self.ms1_inject_vs_ret_full_path, '%s - MS1' % self.sample, 'Retention Time (min)',
                                 [non_maxx[1], maxx[1]], 'Injection Time (msec)', [non_maxx[0], maxx[0]], traces=['',''], axis_range=ranges_dict)


class InjectVsRetModule(QCResourceModule):
    def __init__(self, samples, samples_dict):
        super().__init__(samples, samples_dict)

    def _unite_samples_data(self):
        pass

    def _calc_ranges(self):
        self.ms1_ret_range = self._get_range('ms1_ret')
        self.ms1_inject_range = self._get_range('ms1_inject')
        self.ms1_inject_vs_ret_ranges = {'x': self.ms1_ret_range, 'y': self.ms1_inject_range}
        return self.ms1_inject_vs_ret_ranges

    def _set_resources_paths(self, output_dir):
        pass

    def generate_resources(self, recursive=True):
        if recursive:
            for sample in self.samples:
                self.samples_dict[sample].generate_resources(ranges_dict=self.ms1_inject_vs_ret_ranges)


class TICComponent(QCResourceComponent):
    KEEP_PARAMS = ['run_date', 'ms1_intensities_total_sum']

    def __init__(self, sample, run_date=None, ms1_intensities_total_sum=None):
        super().__init__(sample, keep_params=self.KEEP_PARAMS)
        self.ms1_intensities_sum = []
        self.run_date = run_date if run_date is not None else None
        self.ms1_intensities_total_sum = ms1_intensities_total_sum if ms1_intensities_total_sum is not None else []

    def read_spec(self, spec):
        if spec.ms_level == 1:
            self.ms1_intensities_sum.append(np.sum(spec.intensity_array))

    def finish(self):
        self.ms1_intensities_total_sum = sum(self.ms1_intensities_sum)

    def _arrange_plot_data(self):
        pass

    def _set_resources_paths(self, output_dir):
        pass

    def generate_resources(self, ranges_dict=None):
        pass


class TICModule(QCResourceModule):
    def __init__(self, samples, samples_dict):
        super().__init__(samples, samples_dict)

    def _unite_samples_data(self):
        # sort samples and sums:
        self.sorted_lex_samples = sorted(self.samples)
        self.run_dates = [self.samples_dict[sample].run_date for sample in self.samples]
        self.sorted_run_date_samples = list(
            list(zip(*sorted(zip(self.samples, self.run_dates), key=lambda x: x[1])))[0])
        self.sorted_lex_tic_sums = [self.samples_dict[sample].ms1_intensities_total_sum for sample in
                                    self.sorted_lex_samples]
        self.sorted_run_date_tic_sums = [self.samples_dict[sample].ms1_intensities_total_sum for sample in
                                         self.sorted_run_date_samples]

    def _calc_ranges(self):
        return None

    def _set_resources_paths(self, output_dir):
        self.tic_lex_sort_relative_path = os.path.join(IMAGES, 'tic-lex-sort.png')
        self.tic_lex_sort_full_path = os.path.join(output_dir, self.tic_lex_sort_relative_path)
        # run date sort:
        self.tic_run_date_sort_relative_path = os.path.join(IMAGES, 'tic-run-date-sort.png')
        self.tic_run_date_sort_full_path = os.path.join(output_dir, self.tic_run_date_sort_relative_path)

    def generate_resources(self, recursive=True):
        # TIC:
        tic_figsize = (22, 13.2)
        # lex sort:
        GraphUtils.bar_graph(self.tic_lex_sort_full_path, '', 'Samples', self.sorted_lex_samples, 'MS1 sum intensities',
                             self.sorted_lex_tic_sums, rotation=270, figsize=tic_figsize)
        # run date sort:
        GraphUtils.bar_graph(self.tic_run_date_sort_full_path, '', 'Samples', self.sorted_run_date_samples,
                             'MS1 Intensities Sum', self.sorted_run_date_tic_sums, rotation=270, figsize=tic_figsize)
        if recursive:
            for sample in self.samples:
                self.samples_dict[sample].generate_resources()


class MS2IntensitiesComponent(QCResourceComponent):
    def finish(self):
        pass

    KEEP_PARAMS = ['ms2_max_intensity', 'ms2_num_intensity']

    def __init__(self, sample, ms2_max_intensity=None, ms2_num_intensity=None):
        super().__init__(sample, keep_params=self.KEEP_PARAMS)
        self.ms2_max_intensity = ms2_max_intensity if ms2_max_intensity is not None else []
        self.ms2_num_intensity = ms2_num_intensity if ms2_num_intensity is not None else []

    def read_spec(self, spec):
        if spec.ms_level == 2:
            num_intensities = len(spec.intensity_array)
            self.ms2_num_intensity.append(num_intensities)
            if num_intensities > 0:
                self.ms2_max_intensity.append(max(spec.intensity_array))
            else:
                self.ms2_max_intensity.append(0)

    def _arrange_plot_data(self):
        self.ms2_max_log_intensity = list(np.log10(np.array(self.ms2_max_intensity) + 1))

    def _set_resources_paths(self, output_dir):
        self.ms2_max_intensity_vs_ms2_num_intesities_relative_path = \
            os.path.join(IMAGES, '%s-ms2-max-log-intensity-vs-ms2-num-intensities.png' % self.sample)
        self.ms2_max_intensity_vs_ms2_num_intesities_full_path = \
            os.path.join(output_dir, self.ms2_max_intensity_vs_ms2_num_intesities_relative_path)

    def generate_resources(self, ranges_dict=None):
        GraphUtils.scatter_graph(self.ms2_max_intensity_vs_ms2_num_intesities_full_path, self.sample,
                                 'MS2 most intense fragment (log10)',
                                 self.ms2_max_log_intensity, 'Number of fragments', self.ms2_num_intensity,
                                 density=True, axis_range=ranges_dict)


class MS2IntensitiesModule(QCResourceModule):
    def __init__(self, samples, samples_dict):
        super().__init__(samples, samples_dict)

    def _unite_samples_data(self):
        pass

    def _calc_ranges(self):
        self.ms2_max_log_intensity_range = self._get_range('ms2_max_log_intensity')
        self.ms2_num_intensity_range = self._get_range('ms2_num_intensity')
        self.ms2_intensities_ranges = {'x': self.ms2_max_log_intensity_range, 'y': self.ms2_num_intensity_range}
        return self.ms2_intensities_ranges

    def _set_resources_paths(self, output_dir):
        pass

    def generate_resources(self, recursive=True):
        if recursive:
            for sample in self.samples:
                self.samples_dict[sample].generate_resources(ranges_dict=self.ms2_intensities_ranges)


class MS2PrecRatioComponent(QCResourceComponent):
    def finish(self):
        pass

    KEEP_PARAMS = ['ms2_prec_ratio', 'scan_numbers']

    PREC_RATIO_BOUNDS = [0, 0.5, 1.5]
    PREC_RATIO_LABELS = ['0', '0-0.5', '0.5-1.5', '>1.5']

    def __init__(self, sample, ms2_prec_ratio=None, scan_numbers=None):
        super().__init__(sample, keep_params=self.KEEP_PARAMS)
        self.ms2_prec_ratio = ms2_prec_ratio if ms2_prec_ratio is not None else []
        self.scan_numbers = scan_numbers if scan_numbers is not None else []

    def read_spec(self, spec):
        if spec.ms_level == 2:
            current_ms2_prec_ratio = self._get_ms2_precursor_ratio(spec.spec_id, spec.ms2_mz_value, spec.mz_array,
                                                                   spec.intensity_array)
            if current_ms2_prec_ratio is not None:
                self.ms2_prec_ratio.append(current_ms2_prec_ratio)
                self.scan_numbers.append(spec.scan_number)

    def _get_ms2_precursor_ratio(self, spec_id, ms2_mz_value, ms2_mz_array, ms2_intensities_array):
        current_ms2_prec_ratio = 0
        prec_idx = SearchUtils.binary_search_ppm(ms2_mz_array, ms2_mz_value)
        if prec_idx != -1:
            ms2_prec_intensity = ms2_intensities_array[prec_idx]
            # remove precursor from both arrays in order to find the max intensity value (precursor not included):
            ms2_intensities_array_copy = list(ms2_intensities_array)
            del ms2_intensities_array_copy[prec_idx]
            if not ms2_intensities_array_copy:
                err_msg = "Failed to get precursor ratio because it is the only mz value in the mz array/intensitiy array."
                logger.warning('%s - %s (%s)' % (self.sample, err_msg, spec_id))
                return None
            max_intensity = max(ms2_intensities_array_copy)
            # check if all the intensities array is zeros:
            if max_intensity == 0:
                err_msg = 'Failed to get precursor ratio because MS2 intensities after precursor extraction are equal to 0'
                logger.warning('%s - %s (%s)' % (self.sample, err_msg, spec_id))
                return None
            current_ms2_prec_ratio = ms2_prec_intensity / max_intensity

        return current_ms2_prec_ratio

    def _arrange_plot_data(self):
        self.ms2_prec_ratio_labels = MS2PrecRatioComponent.PREC_RATIO_LABELS
        self.ms2_prec_ratio_sizes = Utils.calc_range_sizes(self.ms2_prec_ratio, MS2PrecRatioComponent.PREC_RATIO_BOUNDS)

    def _set_resources_paths(self, output_dir):
        self.prec_ratio_relative_path = os.path.join(IMAGES, '%s-prec-ratio.png' % self.sample)
        self.prec_ratio_full_path = os.path.join(output_dir, self.prec_ratio_relative_path)

    def generate_resources(self, ranges_dict=None):
        # GraphUtils.pie_graph(self.prec_ratio_full_path, self.sample, self.ms2_prec_ratio_labels, self.ms2_prec_ratio_sizes)
        GraphUtils.bar_graph(self.prec_ratio_full_path, self.sample, 'Precursor ratio',
                             self.ms2_prec_ratio_labels, 'Counts',
                             self.ms2_prec_ratio_sizes, axis_range=ranges_dict)


class MS2PrecRatioModule(QCResourceModule):
    def __init__(self, samples, samples_dict):
        super().__init__(samples, samples_dict)

    def _unite_samples_data(self):
        pass

    def _calc_ranges(self):
        self.ms2_prec_ratio_sizes_range = self._get_range('ms2_prec_ratio_sizes', min_value=0, padding_min=0)
        self.prec_ratio_ranges = {'y': self.ms2_prec_ratio_sizes_range}
        return self.prec_ratio_ranges

    def _set_resources_paths(self, output_dir):
        pass

    def generate_resources(self, recursive=True):
        if recursive:
            for sample in self.samples:
                self.samples_dict[sample].generate_resources(ranges_dict=self.prec_ratio_ranges)


class MZDistComponent(QCResourceComponent):
    def finish(self):
        pass

    KEEP_PARAMS = ['ms2_mz_value']

    # bounds note - each element in the array represents less then and equal, for example [2,4] represents three parts x<=2, 2<x<=4, x<4
    MS2_MZ_VALUES_BOUNDS = list(range(300, 2000, 50))
    MS2_MZ_VALUES_X_AXIS = MS2_MZ_VALUES_BOUNDS + [2000]

    def __init__(self, sample, ms2_mz_value=None):
        super().__init__(sample, keep_params=self.KEEP_PARAMS)
        self.ms2_mz_value = ms2_mz_value if ms2_mz_value is not None else []

    def read_spec(self, spec):
        if spec.ms_level == 2:
            self.ms2_mz_value.append(spec.ms2_mz_value)

    def _arrange_plot_data(self):
        self.ms2_mz_value_sizes = Utils.calc_range_sizes(self.ms2_mz_value, MZDistComponent.MS2_MZ_VALUES_BOUNDS)
        self.ms2_mz_value_x_axis = MZDistComponent.MS2_MZ_VALUES_X_AXIS

    def _set_resources_paths(self, output_dir):
        self.ms2_mz_value_relative_path = os.path.join(IMAGES, '%s-ms2-mz-value.png' % self.sample)
        self.ms2_mz_value_full_path = os.path.join(output_dir, self.ms2_mz_value_relative_path)

    def generate_resources(self, ranges_dict=None):
        GraphUtils.line_graph(self.ms2_mz_value_full_path, self.sample, 'M/Z Value', self.ms2_mz_value_x_axis,
                              'Counts', self.ms2_mz_value_sizes, axis_range=ranges_dict)


class MZDistModule(QCResourceModule):
    def __init__(self, samples, samples_dict):
        super().__init__(samples, samples_dict)

    def _unite_samples_data(self):
        self.ms2_mz_values_x_axis_chunks = self._chunk_field('ms2_mz_value_x_axis')
        self.ms2_mz_values_sizes_chunks = self._chunk_field('ms2_mz_value_sizes')

    def _calc_ranges(self):
        self.ms2_mz_value_sizes_range = self._get_range('ms2_mz_value_sizes', min_value=0, padding_min=0)
        self.ms2_mz_value_ranges = {'y': self.ms2_mz_value_sizes_range}
        return self.ms2_mz_value_ranges

    def _set_resources_paths(self, output_dir):
        self.ms2_mz_value_relative_paths = []
        self.ms2_mz_value_full_paths = []
        for (i, samples_chunk) in enumerate(self.sample_chunks):
            # ms2 mz value graphs:
            self.ms2_mz_value_relative_paths.append(os.path.join(IMAGES, 'all-samples%s-ms2-mz-value.png' % (i + 1)))
            self.ms2_mz_value_full_paths.append(os.path.join(output_dir, self.ms2_mz_value_relative_paths[i]))

    def generate_resources(self, recursive=True):
        for (i, samples_chunk) in enumerate(self.sample_chunks):
            # ms2 mz value graphs:
            GraphUtils.line_graph(self.ms2_mz_value_full_paths[i], 'All Samples%s' % (i + 1), 'M/Z Value',
                                  self.ms2_mz_values_x_axis_chunks[i], 'Counts', self.ms2_mz_values_sizes_chunks[i],
                                  traces=samples_chunk, axis_range=self.ms2_mz_value_ranges)
        if recursive:
            for sample in self.samples:
                self.samples_dict[sample].generate_resources(ranges_dict=self.ms2_mz_value_ranges)


class MZValuePeak(object):
    def __init__(self, mz_value):
        self.mz_value = mz_value
        self.size = 0
        self.intensities = deque()
        self.scan_times = deque()
        self.peak = None
        self.t1 = None
        self.t2 = None

    def add_spec(self, intensity, scan_time):
        self.size += 1
        self.intensities.append(intensity)
        self.scan_times.append(scan_time)

    def add_left_spec(self, intensity, scan_time):
        self.size += 1
        self.intensities.appendleft(intensity)
        self.scan_times.appendleft(scan_time)

    def find_peak_t1_t2(self):
        # get max intensity, then go the both sides (before and after) until it's less then 50% and get the closer to 50% max:
        intensities = list(self.intensities)
        scan_times = list(self.scan_times)
        # find max intensity and its index:
        intensities_len = len(intensities)
        self.peak = 0
        peak_time = 0
        max_intensity_idx = 0
        for (i, intensity) in enumerate(intensities):
            if intensity > self.peak:
                self.peak = intensity
                peak_time = scan_times[i]
                max_intensity_idx = i
        # go right and left until i get to half the max intensity:
        times = []
        half_max_intensity = self.peak / 2
        increments = [-1, 1]
        for increment in increments:
            idx = max_intensity_idx
            while 0 <= idx < intensities_len and intensities[idx] > half_max_intensity:
                idx += increment
            if 0 <= idx < intensities_len:
                times.append(scan_times[idx])
            else:
                return
        self.t1 = peak_time - times[0]
        self.t2 = times[1] - peak_time

    def __str__(self):
        str_repr = 'mz value: %s\npeak: %s\nsize: %s\nintensities: %s\nscan times: %s\n-------------' % (
            self.mz_value, self.peak, self.size, self.intensities, self.scan_times)
        return str_repr


class MZPeaksComponent(QCResourceComponent):
    KEEP_PARAMS = ['log_peak_intensities', 't1_times', 't2_times', 't2_t1_ratio']

    MS1_FORWARDS_INTERVAL = 3
    MS1_BACKWARDS_INTERVAL = 2

    FMHW_BOUNDS = [0.1, 0.2, 0.3, 0.4, 0.5, 1, 1.5, 2]
    FMHW_LABELS = ['0-0.1', '0.1-0.2', '0.2-0.3', '0.3-0.4', '0.4-0.5', '0.5-1', '1-1.5', '1.5-2', '>2']

    PEAK_SYMMETRY_BOUNDS = [0.5, 1, 1.5, 2, 2.5, 3, 4, 5]
    PEAK_SYMMETRY_LABELS = ['0-0.5', '0.5-1', '1-1.5', '1.5-2', '2-2.5', '2.5-3', '3-4', '4-5', '>5']

    def __init__(self, sample, log_peak_intensities=None, t1_times=None, t2_times=None, t2_t1_ratio=None):
        super().__init__(sample, keep_params=self.KEEP_PARAMS)
        self.log_peak_intensities = log_peak_intensities if log_peak_intensities is not None else []
        self.t1_times = t1_times if t1_times is not None else []
        self.t2_times = t2_times if t2_times is not None else []
        self.t2_t1_ratio = t2_t1_ratio if t2_t1_ratio is not None else []
        self.old_ms1_specs = deque()
        self.mz_value_peaks = deque()
        self.final_mz_value_peaks = []

    def read_spec(self, spec):
        if spec.ms_level == 1:
            # MZ values peaks - explore current peaks:
            self._check_mz_peaks(spec.mz_array, spec.intensity_array, spec.scan_start_time)
            # save MS1's 2 minutes back:
            self.old_ms1_specs.appendleft(spec)
            while self.old_ms1_specs[
                -1].scan_start_time < spec.scan_start_time - MZPeaksComponent.MS1_BACKWARDS_INTERVAL:
                self.old_ms1_specs.pop()
        elif spec.ms_level == 2:
            self.mz_value_peaks.append(MZValuePeak(spec.ms2_mz_value))

    def _check_mz_peaks(self, last_ms1_mz_array, last_ms1_intensities_array, scan_start_time):
        num_peaks = len(self.mz_value_peaks)
        for i in range(num_peaks):
            mz_value_peak = self.mz_value_peaks.popleft()
            index = SearchUtils.binary_search_ppm(last_ms1_mz_array, mz_value_peak.mz_value)
            if index != -1:
                # if first, append old ms1's:
                if mz_value_peak.size == 0:
                    for ms1_spec in self.old_ms1_specs:
                        old_index = SearchUtils.binary_search_ppm(ms1_spec.mz_array, mz_value_peak.mz_value)
                        if old_index != -1:
                            mz_value_peak.add_left_spec(ms1_spec.intensity_array[old_index], ms1_spec.scan_start_time)
                        else:
                            break
                # checks if still in ms1's interval:
                if mz_value_peak.size > 0 and scan_start_time > mz_value_peak.scan_times[
                    0] + MZPeaksComponent.MS1_FORWARDS_INTERVAL:
                    self.final_mz_value_peaks.append(mz_value_peak)
                else:
                    mz_value_peak.add_spec(last_ms1_intensities_array[index], scan_start_time)
                    self.mz_value_peaks.append(mz_value_peak)
            else:
                if mz_value_peak.size:
                    self.final_mz_value_peaks.append(mz_value_peak)

    def finish(self):
        # find peaks and filter small peaks (noise):
        max_peak = 0
        for final_mz_value_peak in self.final_mz_value_peaks:
            final_mz_value_peak.find_peak_t1_t2()
            if final_mz_value_peak.peak > max_peak:
                max_peak = final_mz_value_peak.peak
        peak_threshold = max_peak / 1000
        # print(max_peak)
        # print(peak_threshold)
        self.final_mz_value_peaks = [final_mz_value_peak for final_mz_value_peak in self.final_mz_value_peaks
                                     if
                                     final_mz_value_peak.peak > peak_threshold and final_mz_value_peak.t1 is not None]
        # FMHW - prep data for sample qc:
        self.log_peak_intensities = []
        self.t1_times = []
        self.t2_times = []
        self.t2_t1_ratio = []
        for final_mz_value_peak in self.final_mz_value_peaks:
            # print(final_mz_value_peak)
            self.log_peak_intensities.append(np.log10(final_mz_value_peak.peak))
            self.t1_times.append(final_mz_value_peak.t1)
            self.t2_times.append(final_mz_value_peak.t2)
            self.t2_t1_ratio.append(final_mz_value_peak.t2 / final_mz_value_peak.t1)
        self.old_ms1_specs = None

    def _arrange_plot_data(self):
        self.t_sum_times = [self.t1_times[i] + self.t2_times[i] for i in range(len(self.t1_times))]
        # fmhw:
        self.fmhw_labels = MZPeaksComponent.FMHW_LABELS
        self.fmhw_labels_sizes = Utils.calc_range_sizes(self.t1_times, MZPeaksComponent.FMHW_BOUNDS)
        # peak symmetry:
        self.peak_symmetry_labels = MZPeaksComponent.PEAK_SYMMETRY_LABELS
        self.peak_symmetry_sizes = Utils.calc_range_sizes(self.t2_t1_ratio, MZPeaksComponent.PEAK_SYMMETRY_BOUNDS)

    def _set_resources_paths(self, output_dir):
        # FMHW pie:
        self.fmhw_pie_relative_path = os.path.join(IMAGES, '%s-fmhw-pie.png' % self.sample)
        self.fmhw_pie_full_path = os.path.join(output_dir, self.fmhw_pie_relative_path)
        # create FMHW (peak intentsity vs. t1 time) scatter graph:
        self.peak_intentsity_vs_t_sum_relative_path = os.path.join(IMAGES,
                                                                   '%s-peak-intentsity-vs-t-sum.png' % self.sample)
        self.peak_intentsity_vs_t_sum_relative_path_full_path = os.path.join(output_dir,
                                                                             self.peak_intentsity_vs_t_sum_relative_path)
        # create peak symmetry (peak intentsity vs. t2 t1 ratio) scatter graph:
        self.peak_intentsity_vs_t2_t1_ratio_relative_path = os.path.join(IMAGES,
                                                                         '%s-peak-intentsity-vs-t2-t1-ratio.png' % self.sample)
        self.peak_intentsity_vs_t2_t1_ratio_relative_path_full_path = os.path.join(output_dir,
                                                                                   self.peak_intentsity_vs_t2_t1_ratio_relative_path)

    def generate_resources(self, ranges_dict=None):
        fmhw_range = ranges_dict['fmhw'] if ranges_dict else None
        peak_symmetry_range = ranges_dict['peak_symmetry'] if ranges_dict else None
        # create fmhw pie graphs:
        GraphUtils.pie_graph(self.fmhw_pie_full_path, self.sample, self.fmhw_labels, self.fmhw_labels_sizes)

        # create FMHW (peak intentsity vs. t1 time) scatter graph:
        GraphUtils.scatter_graph(self.peak_intentsity_vs_t_sum_relative_path_full_path, self.sample, 'FWHM (min)',
                                 self.t_sum_times, 'Peak Intensity (log10)', self.log_peak_intensities,
                                 density=True, axis_range=fmhw_range)

        # create peak symmetry (peak intentsity vs. t2 t1 ratio) scatter graph:
        GraphUtils.scatter_graph(self.peak_intentsity_vs_t2_t1_ratio_relative_path_full_path, self.sample,
                                 'Tailing ratio',
                                 self.t2_t1_ratio, 'Peak Intensity (log10)', self.log_peak_intensities,
                                 density=True, axis_range=peak_symmetry_range)


class MZPeaksModule(QCResourceModule):
    def __init__(self, samples, samples_dict):
        super().__init__(samples, samples_dict)

    def _unite_samples_data(self):
        # peak symmetry sizes:
        self.peak_symmetries_lables_chunks = self._chunk_field('peak_symmetry_labels')
        self.peak_symmetries_sizes_chunks = self._chunk_field('peak_symmetry_sizes')

    def _calc_ranges(self):
        self.t_sum_times_range = self._get_range('t_sum_times')
        self.log_peak_intensities_range = self._get_range('log_peak_intensities')
        self.t2_t1_ratio_range = self._get_range('t2_t1_ratio')
        self.fmhw_ranges = {'x': self.t_sum_times_range, 'y': self.log_peak_intensities_range}
        self.peak_symmetry_ranges = {'x': self.t2_t1_ratio_range, 'y': self.log_peak_intensities_range}
        self.mz_peaks_ranges = {'fmhw': self.fmhw_ranges, 'peak_symmetry': self.peak_symmetry_ranges}
        return self.mz_peaks_ranges

    def _set_resources_paths(self, output_dir):
        self.peak_symmetry_relative_paths = []
        self.peak_symmetry_full_paths = []
        for (i, samples_chunk) in enumerate(self.sample_chunks):
            self.peak_symmetry_relative_paths.append(os.path.join(IMAGES, 'all-samples%s-peak-symmetry.png' % (i + 1)))
            self.peak_symmetry_full_paths.append(os.path.join(output_dir, self.peak_symmetry_relative_paths[i]))

    def generate_resources(self, recursive=True):
        for (i, samples_chunk) in enumerate(self.sample_chunks):
            # peak symmetry graphs:
            GraphUtils.bar_graph(self.peak_symmetry_full_paths[i], 'All Samples%s' % (i + 1), 'T2/T1',
                                 self.peak_symmetries_lables_chunks[i][0], 'Num Values',
                                 self.peak_symmetries_sizes_chunks[i], traces=samples_chunk)
        if recursive:
            for sample in self.samples:
                self.samples_dict[sample].generate_resources(ranges_dict=self.mz_peaks_ranges)


class MassDeviationElement(JSONable):
    KEEP_PARAMS = ['scan_number', 'mass', 'scan_time', 'ppm_diff', 'approx_mass']

    def __init__(self, scan_number, mass, scan_time, ppm_diff, approx_mass):
        JSONable.__init__(self, keep_params=MassDeviationElement.KEEP_PARAMS)
        self.scan_number = scan_number
        self.mass = mass
        self.scan_time = scan_time
        self.ppm_diff = ppm_diff
        self.approx_mass = approx_mass


class MassDeviationComponent(QCResourceComponent):
    def finish(self):
        pass

    KEEP_PARAMS = ['masses', 'mass_deviation_dict']

    KNOWN_MASSES = [371.10124, 445.12003]
    MAX_MASS_DEVIATION_PPM = 50

    def __init__(self, sample, masses=None, mass_deviation_dict=None):
        super().__init__(sample, keep_params=self.KEEP_PARAMS)
        self.masses = masses if masses is not None else []
        self.mass_deviation_dict = {float(mass): mass_deviation_dict[mass] for mass in mass_deviation_dict} \
            if mass_deviation_dict is not None else {mass: [] for mass in self.masses}

    def read_spec(self, spec):
        if spec.ms_level == 1:
            # mass deviation:
            for mass in self.masses:
                closest_mass_idx = SearchUtils.binary_search_ppm(spec.mz_array, mass,
                                                                 ppm=MassDeviationComponent.MAX_MASS_DEVIATION_PPM)
                if closest_mass_idx != -1:
                    ppm_diff = SearchUtils.get_ppm_diff(mass, spec.mz_array[closest_mass_idx])
                    new_element = MassDeviationElement(spec.scan_number, mass, spec.scan_start_time, ppm_diff,
                                                       spec.mz_array[closest_mass_idx])
                    self.mass_deviation_dict[mass].append(new_element)

    @staticmethod
    def mass_deviation_to_color(deviation):
        if -5 < deviation < 5:
            return '#1e90ff'
        elif -10 < deviation < 10:
            return 'gold'
        else:
            return 'red'

    def _arrange_plot_data(self):
        self.mass_dev_scan_number_dict = {}
        self.mass_dev_scan_time_dict = {}
        self.mass_dev_ppm_diff_dict = {}
        self.mass_dev_approx_mass_dict = {}
        for (i, mass) in enumerate(self.masses):
            self.mass_dev_scan_number_dict[mass] = [mass_dev_element.scan_number for mass_dev_element in
                                                    self.mass_deviation_dict[mass]]
            self.mass_dev_scan_time_dict[mass] = [mass_dev_element.scan_time for mass_dev_element in
                                                  self.mass_deviation_dict[mass]]
            self.mass_dev_ppm_diff_dict[mass] = [mass_dev_element.ppm_diff for mass_dev_element in
                                                 self.mass_deviation_dict[mass]]
            self.mass_dev_approx_mass_dict[mass] = [mass_dev_element.approx_mass for mass_dev_element in
                                                    self.mass_deviation_dict[mass]]

    def _set_resources_paths(self, output_dir):
        self.mass_deviation_relative_paths = []
        self.mass_deviation_full_paths = []
        for (i, mass) in enumerate(self.masses):
            self.mass_deviation_relative_paths.append(
                os.path.join(IMAGES, '%s-mass-deviation%s.png' % (self.sample, (i + 1))))
            self.mass_deviation_full_paths.append(os.path.join(output_dir, self.mass_deviation_relative_paths[i]))

        self.mass_deviation_matrix_relative_path = os.path.join(RESOURCES, '%s-mass-deviation-matrix.tsv' % self.sample)
        self.mass_deviation_matrix_full_path = os.path.join(output_dir, self.mass_deviation_matrix_relative_path)

    def generate_resources(self, ranges_dict=None):
        # create mass deviation line graphs:
        for (i, mass) in enumerate(self.masses):
            range_dict = ranges_dict[mass] if ranges_dict and mass in ranges_dict else None
            color_map = [MassDeviationComponent.mass_deviation_to_color(value) for value in
                         self.mass_dev_ppm_diff_dict[mass]]
            GraphUtils.scatter_graph(self.mass_deviation_full_paths[i], '%s - Mass %s' % (self.sample, mass),
                                     'Retention Time', self.mass_dev_scan_time_dict[mass], 'Deviation (ppm)',
                                     self.mass_dev_ppm_diff_dict[mass], axis_range=range_dict, color_map=color_map)
        # create mass deviation matrix:
        mass_dfs = []
        for mass in self.mass_deviation_dict:
            mass_arr = [mass] * len(self.mass_deviation_dict[mass])
            current_mass_df = pd.DataFrame(
                {'scan': self.mass_dev_scan_number_dict[mass], 'retention': self.mass_dev_scan_time_dict[mass],
                 'mass found': self.mass_dev_approx_mass_dict[mass], 'mass': mass_arr})
            mass_dfs.append(current_mass_df)
        mass_deviation_df = pd.concat(mass_dfs, ignore_index=True)
        mass_deviation_df.pivot_table(index=['scan', 'retention'], columns='mass', values='mass found').to_csv(
            self.mass_deviation_matrix_full_path, sep='\t')


class MassDeviationModule(QCResourceModule):
    def __init__(self, samples, samples_dict):
        super().__init__(samples, samples_dict)
        # get intersection of masses across all samples:
        self.masses = functools.reduce((lambda x, y: x.intersection(y)),
                                       [set(self.samples_dict[sample].masses) for sample in samples])

    def _unite_samples_data(self):
        self.mass_dev_scan_time_dict = {}
        self.mass_dev_ppm_diff_dict = {}
        for mass in self.masses:
            self.mass_dev_scan_time_dict[mass] = [self.samples_dict[sample].mass_dev_scan_time_dict[mass] for sample in
                                                  self.samples]
            self.mass_dev_ppm_diff_dict[mass] = [self.samples_dict[sample].mass_dev_ppm_diff_dict[mass] for sample in
                                                 self.samples]
        self.mass_dev_scan_time_chunks_dict = {}
        self.mass_dev_ppm_diff_chunks_dict = {}
        for mass in self.masses:
            self.mass_dev_scan_time_chunks_dict[mass] = Utils.list_chunks(self.mass_dev_scan_time_dict[mass],
                                                                          self.SAMPLES_CHUNK_SIZE)
            self.mass_dev_ppm_diff_chunks_dict[mass] = Utils.list_chunks(self.mass_dev_ppm_diff_dict[mass],
                                                                         self.SAMPLES_CHUNK_SIZE)

    def _calc_ranges(self):
        # mass deviation:
        self.mass_range_dict = {}
        for mass in self.masses:
            mass_ret_range = self._get_range(self.mass_dev_scan_time_dict[mass])
            mass_deviation_ret = self._get_range(self.mass_dev_ppm_diff_dict[mass])
            self.mass_range_dict[mass] = {'x': mass_ret_range, 'y': mass_deviation_ret}
        return self.mass_range_dict

    def _set_resources_paths(self, output_dir):
        # mass deviation:
        self.mass_deviation_relative_paths_dict = {}
        self.mass_deviation_full_paths_dict = {}
        for (mass_idx, mass) in enumerate(self.masses):
            self.mass_deviation_relative_paths_dict[mass] = []
            self.mass_deviation_full_paths_dict[mass] = []
            for (i, samples_chunk) in enumerate(self.sample_chunks):
                self.mass_deviation_relative_paths_dict[mass].append(
                    os.path.join(IMAGES, 'all-samples%s-mass%s-deviation.png' % (i + 1, mass_idx + 1)))
                self.mass_deviation_full_paths_dict[mass].append(
                    os.path.join(output_dir, self.mass_deviation_relative_paths_dict[mass][i]))

    def generate_resources(self, recursive=True):
        # mass deviation:
        for (mass_idx, mass) in enumerate(self.masses):
            for (i, samples_chunk) in enumerate(self.sample_chunks):
                color_maps = [[MassDeviationComponent.mass_deviation_to_color(value) for value in arr] for arr in
                              self.mass_dev_ppm_diff_chunks_dict[mass][i]]
                GraphUtils.scatter_subplots_graph(self.mass_deviation_full_paths_dict[mass][i], 'Mass %s' % mass,
                                                  'Deviation (ppm)', self.mass_dev_ppm_diff_chunks_dict[mass][i],
                                                  'Retention time', self.mass_dev_scan_time_chunks_dict[mass][i],
                                                  traces=self.sample_chunks[i], axis_range=None, color_maps=color_maps,
                                                  figsize=(19, 11))
        if recursive:
            for sample in self.samples:
                self.samples_dict[sample].generate_resources(ranges_dict=self.mass_range_dict)


class PeakSplitComponent(QCComponent):
    KEEP_PARAMS = ['total_counts', 'twin_count', 'peak_split']
    NUM_SCANS = 2000
    CUT_OFF = 4.5
    INTENSITY_DIFF = 0.25

    def __init__(self, sample, total_counts, twin_count=None, peak_split=None):
        super().__init__(sample, keep_params=self.KEEP_PARAMS)
        self.ms_count = 0
        self.total_counts = total_counts
        self.lower_bound = (self.total_counts / 2) - PeakSplitComponent.NUM_SCANS
        self.upper_bound = (self.total_counts / 2) + PeakSplitComponent.NUM_SCANS
        self.twin_count = twin_count if twin_count is not None else []
        self.peak_split = peak_split if peak_split is not None else None

    @property
    def peak_split_bool(self):
        return self.peak_split > PeakSplitComponent.CUT_OFF

    def read_spec(self, spec):
        self.ms_count += 1
        if spec.ms_level == 2 and self.lower_bound < self.ms_count < self.upper_bound:
            tuples = list(zip(spec.mz_array, spec.intensity_array))
            tuples.sort(key=lambda x: x[1], reverse=True)
            top50_tuples = tuples[:50]
            top50_tuples.sort(key=lambda x: x[0])
            count = 0
            for i in range(2, len(top50_tuples)):
                intensity_cond = abs(top50_tuples[i - 1][1] - top50_tuples[i][1]) < (
                        top50_tuples[i - 1][1] + top50_tuples[i][1]) * 0.5 * PeakSplitComponent.INTENSITY_DIFF
                mz_cond = 50 < abs(SearchUtils.get_ppm_diff(top50_tuples[i - 1][0], top50_tuples[i][0])) < 300
                if intensity_cond and mz_cond:
                    count += 1
            self.twin_count.append(count)

    def finish(self):
        self.peak_split = float(sum(self.twin_count)) / max(len(self.twin_count), 1)
        if self.peak_split_bool:
            logger.info('%s - found peak split with value: %s' % (self.sample, self.peak_split))
        # print('%s - peak split list: %s' % (self.sample, self.twin_count))
        # print('%s - avg peak split: %s' % (self.sample, self.peak_split))


class IDFolderData:
    PSM_FILE = 'psm.tsv'
    PEPTIDE_FILE = 'peptide.tsv'
    PROTEIN_FILE = 'protein.tsv'

    def __init__(self, id_folder_path):
        self.psm_data_frame = pd.read_csv(os.path.join(id_folder_path, self.PSM_FILE), sep='\t')
        self.peptide_data_frame = pd.read_csv(os.path.join(id_folder_path, self.PEPTIDE_FILE), sep='\t')
        self.protein_data_frame = pd.read_csv(os.path.join(id_folder_path, self.PROTEIN_FILE), sep='\t')


class FragPipeStatsComponent(QCComponent):
    KEEP_PARAMS = ['psm_count', 'peptide_count', 'protein_count']

    def __init__(self, sample, id_instance: IDFolderData = None, psm_count=None, peptide_count=None, protein_count=None):
        super().__init__(sample, keep_params=self.KEEP_PARAMS)
        self.id_data_instance = id_instance
        self.psm_count = psm_count
        self.peptide_count = peptide_count
        self.protein_count = protein_count

    def finish(self):
        self.psm_count = len(self.id_data_instance.psm_data_frame.index)
        self.peptide_count = len(self.id_data_instance.peptide_data_frame.index)
        self.protein_count = len(self.id_data_instance.protein_data_frame.index)

    def read_spec(self, spec):
        pass


class MassErrorComponent(QCComponent):
    KEEP_PARAMS = ['root_mean_square', 'standard_deviation']

    def __init__(self, sample, id_instance: IDFolderData = None, root_mean_square=None, standard_deviation=None):
        super().__init__(sample, keep_params=self.KEEP_PARAMS)
        self.sample = sample
        self.id_data_instance = id_instance
        self.root_mean_square = root_mean_square
        self.standard_deviation = standard_deviation

    def finish(self):
        data_frame = self.id_data_instance.psm_data_frame
        if data_frame is not None:
            self.root_mean_square = 0
            self.standard_deviation = 0
            if len(data_frame.index) > 0:
                delta_mass_values = data_frame['Delta Mass'].values
                self.root_mean_square = format(np.sqrt(np.mean(delta_mass_values ** 2)), '.4f')
                self.standard_deviation = format(np.std(delta_mass_values), '.4f')
        else:
            raise Exception(f'File {self.id_data_instance.PSM_FILE} in folder {self.sample}_id must have column name Delta Mass')

    def read_spec(self, spec):
        pass


class MissedCleavagesComponent(QCComponent):
    KEEP_PARAMS = ['missed_cleavages']

    def __init__(self, sample, id_instance: IDFolderData = None, missed_cleavages=None):
        super().__init__(sample, keep_params=self.KEEP_PARAMS)
        self.id_data_instance = id_instance
        self.missed_cleavages = missed_cleavages

    # @staticmethod
    # def _calculate_missed_cleavages(peptide: str):
    #     char_count = Counter(peptide[:-1])
    #     r_count = char_count.get('R') if char_count.get('R') is not None else 0
    #     k_count = char_count.get('K') if char_count.get('K') is not None else 0
    #     return r_count + k_count

    def finish(self):
        psm_df = self.id_data_instance.psm_data_frame
        missed_cleavages = 0
        if len(psm_df.index) > 0:
            missed_cleavages = (psm_df[psm_df['Number of Missed Cleavages'] == 0]['Number of Missed Cleavages'].count() /
                                psm_df['Number of Missed Cleavages'].count()) * 100
        self.missed_cleavages = f"{format(missed_cleavages, '.4f')}%"

    def read_spec(self, spec):
        pass


def initialize_components(components_list: List[str], params_dict):
    initialized_components = {}
    id_instance = None
    sample_name = params_dict['sample_name']
    try:
        if params_dict['id_folder']:
            id_instance = IDFolderData(params_dict['id_folder'])
        else:
            logger.warning(f'Missing id folder for sample {sample_name}')
    except FileNotFoundError:
        logger.warning(f'Missing files in {params_dict["id_folder"]}')
    finally:
        for component in components_list:
            if component == TIC:
                initialized_components[component] = COMPONENTS_DICT[component](sample_name, params_dict['run_date'])
            elif component == MASS_DEVIATION:
                initialized_components[component] = COMPONENTS_DICT[component](sample_name, params_dict['masses'])
            elif component == PEAK_SPLIT:
                initialized_components[component] = COMPONENTS_DICT[component](sample_name, params_dict['total_counts'])
            elif component in ID_COMPONENT_DICT:
                if id_instance:
                    initialized_components[component] = ID_COMPONENT_DICT[component](sample_name, id_instance)
            else:
                initialized_components[component] = COMPONENTS_DICT[component](sample_name)

    return initialized_components


MS2_COUNTS = 'ms2_counts'
TOP_N = 'top_n'
CHARGE_DIST = 'charge_dist'
INJECT_TIME = 'inject_time'
RET_VS_TOP_N = 'ret_vs_top_n'
INJECT_VS_RET = 'inject_vs_ret'
TIC = 'total_ion_current'
MS2_INTENSITIES = 'ms2_intensities'
PREC_RATIO = 'prec_ratio'
MZ_DIST = 'mz_dist'
MZ_PEAKS = 'mz_peaks'
MASS_DEVIATION = 'mass_deviation'
PEAK_SPLIT = 'peak_splits'
FRAG_PIPE_STATS = 'frag_pipe_stats'
MASS_ERROR = 'mass_error'
MISSED_CLEAVAGES = 'missed_cleavages'

ID_COMPONENT_DICT: {str: QCComponent} = {FRAG_PIPE_STATS: FragPipeStatsComponent, MASS_ERROR: MassErrorComponent,
                                         MISSED_CLEAVAGES: MissedCleavagesComponent}

COMPONENTS_DICT: {str: QCComponent} = {MS2_COUNTS: MS2CountsComponent, TOP_N: TopNComponent,
                                       CHARGE_DIST: ChargeDistComponent,
                                       INJECT_TIME: MS2InjectTimeComponent, RET_VS_TOP_N: RetVsTopNComponent,
                                       INJECT_VS_RET: InjectVsRetComponent, TIC: TICComponent,
                                       MS2_INTENSITIES: MS2IntensitiesComponent,
                                       PREC_RATIO: MS2PrecRatioComponent, MZ_DIST: MZDistComponent,
                                       MZ_PEAKS: MZPeaksComponent,
                                       MASS_DEVIATION: MassDeviationComponent, PEAK_SPLIT: PeakSplitComponent}

MODULES_DICT: {str: QCModule} = {MS2_COUNTS: QCModule, TOP_N: TopNModule, CHARGE_DIST: ChargeDistModule,
                                 INJECT_TIME: MS2InjectTimeModule, RET_VS_TOP_N: RetVsTopNModule,
                                 INJECT_VS_RET: InjectVsRetModule,
                                 TIC: TICModule, MS2_INTENSITIES: MS2IntensitiesModule, PREC_RATIO: MS2PrecRatioModule,
                                 MZ_DIST: MZDistModule, MZ_PEAKS: MZPeaksModule, MASS_DEVIATION: MassDeviationModule,
                                 PEAK_SPLIT: QCModule, FRAG_PIPE_STATS: QCModule,
                                 MASS_ERROR: QCModule, MISSED_CLEAVAGES: QCModule}
