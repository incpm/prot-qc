# logging
import logging.handlers
import os
import subprocess
import sys
import time
import traceback
from logging.config import dictConfig
from subprocess import Popen

import dask
import dask.multiprocessing
import dask.threaded

from protqc import settings
from protqc.settings import VERSION
from protqc.vendors import is_thermo_supported, get_file_vendor

logger = logging.getLogger(__name__)

from protqc.analyzer import QCAnalyzer
from protqc.reporter import QCReporter
from protqc.settings import RESOURCES_DIR

from protqc.settings import IS_GUI
if IS_GUI:
    import wx


def get_help_bmp():
    help_bmp = wx.Bitmap(os.path.join(RESOURCES_DIR, 'images', 'help-icon-24.png'), wx.BITMAP_TYPE_PNG)
    return help_bmp


# Only for Windows!
class MSConverter(object):
    def __init__(self, msconvert_path):
        self.msconvert_path = msconvert_path

    def _run_msconvert(self, file_path, output_dir):
        cmd = '"%s" -z --ignoreUnknownInstrumentError --inten64 "%s" -o "%s"' % (self.msconvert_path, file_path, output_dir)
        logger.info('Running MSConvert for file: %s' % file_path)
        logger.info('Please wait...')
        p = Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        ret_code = p.wait()
        stdout, stderr = p.communicate()
        if ret_code == 0:
            logger.info('Finished running MSConvert for file: %s' % file_path)
        else:
            logger.error('Failed Running MSConvert for file: %s' % file_path)
            raise Exception(
                'Failed Running MSConvert for file: %s, stdout:%s, stderr: %s' % (file_path, stdout, stderr))

    def _is_convert_needed(self, input_dir, name, file_vendor):
        # no need to convert if there is a json file already:
        if os.path.exists(os.path.join(input_dir, '%s.json' % name)):
            return False
        # no need to convert if it's windows and the vendor is supported:
        if file_vendor == 'thermo' and is_thermo_supported():
            return False
        # else - if there isn't an mzML file already, do conversion:
        else:
            return not os.path.exists(os.path.join(input_dir, '%s.mzML' % name)) \
                   and not os.path.exists(os.path.join(input_dir, '%s.mzml' % name))

    def run_msconvert_on_dir(self, input_dir, num_processes):
        # check which files need to be msconverted:
        cmds = []
        logger.info('Checking if need to run MSConvert...')
        for file in os.listdir(input_dir):
            file_vendor = get_file_vendor(file)
            if file_vendor is not None:
                name = os.path.splitext(os.path.basename(file))[0]
                if self._is_convert_needed(input_dir, name, file_vendor):
                    cmds.append(dask.delayed(self._run_msconvert)(os.path.join(input_dir, file), input_dir))
        # run msconvert on files:
        dask.compute(cmds, scheduler='threads', num_workers=num_processes)


class QCRunner(object):
    def __init__(self, input, output_dir, num_processes=1, id_dir=None, batch=False, masses=None, msconvert_path=None,
                 logger_handlers=None, no_report=False, cmd=None):
        self.id_dir = id_dir
        self.num_workers = num_processes
        self.batch = batch
        self.input = input
        self.output_dir = output_dir
        self.masses = masses
        self.msconvert_path = msconvert_path
        self.no_report = no_report
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)
            logger.info('Creating output directory %s' % output_dir)
        else:
            logger.info('Output directory %s already exists' % output_dir)
        self._init_logger(os.path.join(self.output_dir, 'qc-log.txt'), logger_handlers=logger_handlers)
        logger.info('-=Starting Raw Beans %s=-' % VERSION)
        if cmd is not None:
            logger.info('cmd: %s' % cmd)

    def _init_logger(self, log_filename, logger_handlers=None):
        logging_settings = settings.LOGGING_CONFIG.copy()
        if log_filename:
            logging_settings['handlers']['log_file']['filename'] = log_filename
        dictConfig(logging_settings)
        if logger_handlers:
            for h in logger_handlers:
                logging.getLogger().addHandler(h)

    def run(self):
        exit_status = 0
        start_time = time.time()

        try:
            # if needed, run msconvert (only on windows gui, supports only one input dir!):
            if self.msconvert_path and os.name == 'nt':
                msconverter = MSConverter(self.msconvert_path)
                msconverter.run_msconvert_on_dir(self.input[0], self.num_workers)
            # run protqc:
            qc_analyzer = QCAnalyzer(self.input, self.output_dir, self.id_dir, masses=self.masses)
            samples_json_dict = qc_analyzer.analyze(num_workers=self.num_workers)
            # create report if needed:
            if not self.no_report:
                qc_reporter = QCReporter(samples_json_dict, self.output_dir)
                qc_reporter.create_qc_report(batch=self.batch, num_workers=self.num_workers)
        except Exception as e:
            logger.error(traceback.format_exc())
            exit_status = 1

        end_time = time.time()
        total_time = end_time - start_time
        logger.info('-----------------------------')
        logger.info('Total Runtime: %.2f seconds' % total_time)
        if exit_status:
            logger.info('-=Raw Beans failed!=-')
            sys.exit(exit_status)
        else:
            logger.info('-=Raw Beans finished successfully!=-')


