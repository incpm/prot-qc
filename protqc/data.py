import os
import multiprocessing
from typing import Dict

from protqc.multiprocessing import start_message_queue, STOP_THREAD, get_child_logger
from protqc.json_utils import JSONable
from protqc.modules import Resourceable, MODULES_DICT, QCResourceComponent, QCResourceModule

# logging
import logging
logger = logging.getLogger(__name__)


class SampleQCData(JSONable, Resourceable):
    KEEP_PARAMS = ['sample', 'components_dict']

    def __init__(self, sample, components_dict):
        Resourceable.__init__(self)
        JSONable.__init__(self, keep_params=SampleQCData.KEEP_PARAMS)
        self.sample = sample
        self.components_dict = components_dict

    def prep_resources(self, output_dir):
        for component in self.components_dict.values():
            if isinstance(component, QCResourceComponent):
                component.prep_resources(output_dir)

    def generate_resources(self, ranges_dict=None):
        for component in self.components_dict:
            if isinstance(self.components_dict[component], QCResourceComponent):
                comp_ranges_dict = ranges_dict[component] if ranges_dict else None
                self.components_dict[component].generate_resources(ranges_dict=comp_ranges_dict)

    @staticmethod
    def generate_resources_from_file(json_file, output_dir, ranges_dict, queue=None):
        logger = get_child_logger(queue, __name__)
        sample_qc_data = SampleQCData.from_json_file(json_file)
        sample_qc_data.prep_resources(output_dir)
        sample_qc_data.generate_resources(ranges_dict=ranges_dict)


class QCData(JSONable, Resourceable):
    KEEP_PARAMS = ['samples', 'samples_dict', 'components']

    def __init__(self, samples_dict: Dict[str, SampleQCData]):
        Resourceable.__init__(self)
        JSONable.__init__(self, keep_params=self.KEEP_PARAMS)
        self.samples = list(samples_dict.keys())
        self.samples_dict = samples_dict
        self.components = self._get_samples_components()

    def _get_samples_components(self):
        return {component for sample_qc in self.samples_dict.values() for component in sample_qc.components_dict}

    def save_samples_jsons(self, output_dir, overwrite=True):
        logger.info('Saving json files...')
        for sample_name in self.samples:
            json_path = os.path.join(output_dir, '%s.json' % sample_name)
            if overwrite is True or not os.path.exists(json_path):
                self.samples_dict[sample_name].to_json_file(json_path)

    def _create_modules(self):
        modules_dict = {component: {} for component in self.components}
        for sample_qc_data in self.samples_dict.values():
            for component in sample_qc_data.components_dict:
                modules_dict[component][sample_qc_data.sample] = sample_qc_data.components_dict[component]
        # for component in self.components:
        #     for sample in self.samples:
        #         modules_dict[component][sample] = self.samples_dict[sample].components_dict[component]
        # transform to QCMoudle objects:
        self.modules_dict = {component: MODULES_DICT[component]([component.sample for component in modules_dict[component].values()], modules_dict[component]) for component in modules_dict}

    def prep_resources(self, output_dir):
        logger.info('Preparing resources...')
        self.output_dir = output_dir
        self._create_modules()
        self.ranges_dict = {}
        for component in self.components:
            if isinstance(self.modules_dict[component], QCResourceModule):
                self.ranges_dict[component] = self.modules_dict[component].prep_resources(output_dir)

    def generate_resources(self, num_workers=1):
        logger.info('Generating resources...')
        """
        in order to use multiprocessing, you need to call save_samples_jsons beforehand.
        :param num_workers: num of workers for parallel processing.
        :return:
        """
        # generate modules resources:
        for component in self.components:
            if isinstance(self.modules_dict[component], QCResourceModule):
                self.modules_dict[component].generate_resources(recursive=False)

        # generate samples resources:
        if num_workers == 1:
            for sample in self.samples:
                self.samples_dict[sample].generate_resources(ranges_dict=self.ranges_dict)
        else:
            # open pool and generate graphs in parallel:
            _, queue = start_message_queue(num_workers=num_workers)
            pool = multiprocessing.Pool(num_workers)
            workers_args = [(os.path.join(self.output_dir, '%s.json' % sample), self.output_dir, self.ranges_dict, queue) for sample in self.samples]
            try:
                pool.starmap(SampleQCData.generate_resources_from_file, workers_args)
            except Exception:
                raise
            finally:
                # end queue messages thread if needed:
                if queue:
                    queue.put(STOP_THREAD)
            pool.close()
            pool.join()
