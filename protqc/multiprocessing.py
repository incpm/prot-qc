from threading import Thread
import multiprocessing
import dask
import dask.multiprocessing

# logging
import logging.handlers
logger = logging.getLogger(__name__)


# multiprocessing message queue handling:
STOP_THREAD = 'STOP'


# you have to use this function to get the logger in a multiprocessing function!
def get_child_logger(queue, name):
    # if multiprocessing - do not move this logging part!:
    if queue:
        # remove duplicate logging messages from root logger:
        root_logger = logging.getLogger()
        root_logger.handlers = []
        logger = logging.getLogger(name)
        logger.handlers = []
        # add queue handler to root logger:
        h = logging.handlers.QueueHandler(queue)  # Just the one queue handler needed
        h.setLevel(logging.INFO)
        root_logger.addHandler(h)
        root_logger.setLevel(logging.INFO)
    else:
        logger = logging.getLogger(name)
    return logger


def check_queue_msgs(queue):
    # get logger message from sub-processes:
    while True:
        record = queue.get()
        if record == STOP_THREAD:
            break
        logger.handle(record)


def start_message_queue(num_workers=1):
    # handle multiprocessing logging:
    if num_workers > 1:
        # logging from sub processes:
        # scheduler = dask.multiprocessing.get
        scheduler = 'processes'
        manager = multiprocessing.Manager()
        queue = manager.Queue()
        qc_thread = Thread(target=check_queue_msgs, args=(queue,))
        # qc_thread.daemon = True
        qc_thread.start()
    else:
        # scheduler = dask.get
        scheduler = 'single-threaded'
        queue = None
    return scheduler, queue