import os
from jinja2 import Template
from shutil import copytree, rmtree

# logging
import logging

from protqc.data import QCData, SampleQCData
from protqc.settings import RESOURCES, RESOURCES_DIR, QC_REPORT_TEMPLATE

logger = logging.getLogger(__name__)


def remove_file(file_path):
    try:
        os.remove(file_path)
    except OSError:
        pass


class QCReporter(object):
    def __init__(self, sample_json_dict, output_dir):
        # create output dir if not exist:
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        # else:
        #     logger.info('Output directory %s already exists' % output_dir)
        self.output_dir = output_dir
        # creating qc data from sample jsons:
        logger.info('Loading all samples data from jsons...')
        samples_dict = {sample_name: SampleQCData.from_json_file(sample_json_dict[sample_name]) for sample_name in
                        sample_json_dict}
        qc_data = QCData(samples_dict)
        logger.info('Finished Loading all samples data from jsons')
        self.qc_data = qc_data

    def _prep_report(self, output_dir):
        resources_dir = os.path.join(output_dir, RESOURCES)
        # remove old resource folder if exist and copy the copy resource folder again:
        rmtree(resources_dir, ignore_errors=True)
        copytree(RESOURCES_DIR, resources_dir)
        # remove old report files:
        qc_report_path = os.path.join(output_dir, 'qc-report.html')
        static_qc_report_path = os.path.join(output_dir, 'static-qc-report.html')
        remove_file(qc_report_path)
        remove_file(static_qc_report_path)
        return qc_report_path, static_qc_report_path

    def create_qc_report(self, batch=False, num_workers=1):
        # read the protqc report template
        with open(QC_REPORT_TEMPLATE, 'r') as template_file:
            template = Template(template_file.read())

        # if batch - create report for each sample, if not - create one report to all samples:
        if batch:
            all_samples = self.qc_data.samples
            for sample_name in all_samples:
                # remove old resource folder if exist and copy the copy resource folder again:
                sample_output_dir = os.path.join(self.output_dir, sample_name)
                sample_qc_report_path, sample_static_qc_report_path = self._prep_report(sample_output_dir)
                # generate resources:
                self.qc_data.samples = [sample_name]
                self.qc_data.prep_resources(sample_output_dir)
                self.qc_data.generate_resources()
                # fill the template with data for report files:
                qc_report_text = template.render(static=False, **self.qc_data.modules_dict)
                static_qc_report_text = template.render(static=True, **self.qc_data.modules_dict)
                # write report files:
                logger.info("Creating QC Report for sample %s..." % sample_name)
                with open(sample_qc_report_path, 'w') as qc_report_handle:
                    qc_report_handle.write(qc_report_text)
                with open(sample_static_qc_report_path, 'w') as qc_report_handle:
                    qc_report_handle.write(static_qc_report_text)
            # change value back to all samples:
            self.qc_data.samples = all_samples
        else:
            # remove old resource folder if exist and copy the copy resource folder again:
            qc_report_path, static_qc_report_path = self._prep_report(self.output_dir)
            # generate resources:
            self.qc_data.prep_resources(self.output_dir)
            self.qc_data.generate_resources(num_workers=num_workers)
            # fill the template with data for report files:
            qc_report_text = template.render(static=False, **self.qc_data.modules_dict)
            static_qc_report_text = template.render(static=True, **self.qc_data.modules_dict)
            # write the filled template files:
            logger.info("Creating QC Report...")
            with open(qc_report_path, 'w') as qc_report_handle:
                qc_report_handle.write(qc_report_text)
            with open(static_qc_report_path, 'w') as qc_report_handle:
                qc_report_handle.write(static_qc_report_text)
