import os
import logging
import pkg_resources


VERSION = pkg_resources.get_distribution("protqc").version
PROJECT_ROOT = os.path.abspath(os.path.join(__name__, os.pardir))

# dirs and files:
REPORTS = 'reports/qc-report'
RESOURCES = 'resources'
IMAGES = os.path.join(RESOURCES, 'images')
REPORTS_DIR = os.path.join(os.path.dirname(__file__), REPORTS)
RESOURCES_DIR = os.path.join(REPORTS_DIR, RESOURCES)
QC_REPORT_TEMPLATE = os.path.join(REPORTS_DIR, 'qc-report.html')

# logging:
LOGGER_NAME = 'QC'
LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '%(asctime)s - [%(levelname)s] - %(name)s - %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
        },
        'log_file': {
            'level': 'INFO',
            'formatter': 'standard',
            'class': 'logging.FileHandler',
            # missing filename - should be set before using this handler
        },
    },
    'loggers': {
        '': {
            'handlers': ['console', 'log_file'],
            'level': 'INFO',
            'propagate': True
        },
    }
}

# gui:
IS_GUI = False


def set_is_gui(value):
    global IS_GUI
    IS_GUI = value
