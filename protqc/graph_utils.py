import numpy as np
import traceback

# plotting:
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import seaborn as sns
plt.style.use('seaborn')

import logging
logger = logging.getLogger(__name__)


class GraphUtils(object):
    PLOT_WIDTH = 7.1875
    PLOT_HEIGHT = 4.6875
    # COLORS = cm.rainbow(np.linspace(0, 1, 10))
    COLORS = ['#1e90ff', 'red', 'gold', 'lightcoral', 'lightskyblue', 'yellowgreen', 'pink', 'darkgreen', 'yellow',
              'grey','violet','magenta','cyan']
    MARKER_SIZE = 10
    ALPHA = 0.8

    @staticmethod
    def adjust_ranges(ranges):
        if ranges:
            if 'x' in ranges and ranges['x'] is not None:
                plt.xlim(ranges['x'])
            if 'y' in ranges and ranges['y'] is not None:
                plt.ylim(ranges['y'])

    @staticmethod
    def scatter_graph(file_path, title, x_title, x, y_title, y, traces=None, axis_range=None, density=False, figsize=(PLOT_WIDTH,PLOT_HEIGHT), color_map=None):
        try:
            plt.figure(figsize=figsize)
            if len(x) > 0 and isinstance(x[0], list):
                for i in range(len(x)):
                    plt.scatter(x[i], y[i], marker="o", s=GraphUtils.MARKER_SIZE, color=GraphUtils.COLORS[i], label=traces[i])
                    if density:
                        sns.kdeplot(x[i], y[i], cmap="Reds", shade=False)
            else:
                if color_map is not None:
                    color = color_map
                else:
                    color = GraphUtils.COLORS[0]
                plt.scatter(x, y, marker="o", s=GraphUtils.MARKER_SIZE, color=color)
                if density:
                    sns.kdeplot(x, y, cmap="Reds", shade=False)
            plt.legend()
            plt.title(title)
            plt.xlabel(x_title)
            plt.ylabel(y_title)
            GraphUtils.adjust_ranges(axis_range)
            plt.tight_layout()
            plt.savefig(file_path)
            plt.close()
        except Exception:
            logger.error('An exception was raised while creating graph: %s, (traceback below). trying to continue' % file_path)
            logger.error(traceback.format_exc())

    @staticmethod
    def scatter_subplots_graph(file_path, title, x_title, x, y_title, y, traces=None, axis_range=None, density=False,
                      figsize=(PLOT_WIDTH, PLOT_HEIGHT), color_maps=None, margin=0.05):
        try:
            plt.figure(figsize=figsize)
            plt.suptitle(title, size=17)
            num_samples = len(x)
            for i in range(len(x)):
                ax = plt.subplot(1, num_samples, (i + 1))
                color = color_maps[i] if color_maps else GraphUtils.COLORS[0]
                plt.scatter(x[i], y[i], marker="o", s=GraphUtils.MARKER_SIZE, color=color)
                plt.text(1, 1, traces[i], verticalalignment='top', rotation=270, transform=ax.transAxes, fontsize=12)
                plt.xlabel(x_title)
                plt.ylabel(y_title)
                GraphUtils.adjust_ranges(axis_range)
                if density:
                    sns.kdeplot(x[i], y[i], cmap="Reds", shade=False)
            plt.tight_layout()
            plt.subplots_adjust(bottom=margin, right=1-margin, left=margin, top=1-margin)
            plt.savefig(file_path)
            plt.close()
        except Exception:
            logger.error('An exception was raised while creating graph: %s, (traceback below). trying to continue' % file_path)
            logger.error(traceback.format_exc())

    @staticmethod
    def line_graph(file_path, title, x_title, x, y_title, y, traces=None, axis_range=None, legend_loc=1, legend_fontsize=8, figsize=(PLOT_WIDTH, PLOT_HEIGHT), spacing=None):
        try:
            plt.figure(figsize=figsize)
            if len(x) > 0 and isinstance(x[0], list):
                x_axis = x[0]
                for i in range(len(x)):
                    plt.plot(x[i], y[i], markersize=GraphUtils.MARKER_SIZE, color=GraphUtils.COLORS[i], label=traces[i])
            else:
                x_axis = x
                plt.plot(x, y, markersize=GraphUtils.MARKER_SIZE, color=GraphUtils.COLORS[0])
            if spacing:
                plt.gca().set_xticks(x_axis[::spacing])
            plt.legend(loc=legend_loc, prop={'size': legend_fontsize})
            plt.title(title)
            plt.xlabel(x_title)
            plt.ylabel(y_title)
            GraphUtils.adjust_ranges(axis_range)
            plt.tight_layout()
            plt.savefig(file_path)
            plt.close()
        except Exception:
            logger.error('An exception was raised while creating graph: %s, (traceback below). trying to continue' % file_path)
            logger.error(traceback.format_exc())

    @staticmethod
    def hist_graph(file_path, title, x_title, x, y_title, traces=None, axis_range=None, bins=None, figsize=(PLOT_WIDTH,PLOT_HEIGHT)):
        try:
            plt.figure(figsize=figsize)
            bins_value = matplotlib.rcParams['hist.bins'] if bins is None else bins
            if len(x) > 0 and isinstance(x[0], list):
                plt.hist(x, color=GraphUtils.COLORS[:len(x)], label=traces[:len(x)], bins=bins_value)
            else:
                # print(Counter(x))
                plt.hist(x, color=GraphUtils.COLORS[0], bins=bins_value)
            plt.legend()
            plt.title(title)
            plt.xlabel(x_title)
            plt.ylabel(y_title)
            if axis_range is not None and 'x' in axis_range:
                new_axis_range = dict(axis_range)
                new_axis_range['x'] = (axis_range['x'][0] - 1, axis_range['x'][1] + 1)
                axis_range = new_axis_range
            GraphUtils.adjust_ranges(axis_range)
            plt.tight_layout()
            plt.savefig(file_path)
            plt.close()
        except Exception:
            logger.error('An exception was raised while creating graph: %s, (traceback below). trying to continue' % file_path)
            logger.error(traceback.format_exc())

    @staticmethod
    def bar_graph(file_path, title, x_title, x_labels, y_title, sizes, traces=None, axis_range=None, spacing=1,
                  rotation=0, legend_loc=1, legend_fontsize=8, figsize=(PLOT_WIDTH,PLOT_HEIGHT)):
        try:
            plt.figure(figsize=figsize)
            x = list(range(len(x_labels)))
            if len(sizes) > 0 and isinstance(sizes[0], list):
                width = 0.8 / len(sizes)
                start_pos = -0.3
                x = np.array(x)
                for i in range(len(sizes)):
                    plt.bar(x + start_pos + (width * i), height=sizes[i], width=width, color=GraphUtils.COLORS[i], label=traces[i])
            else:
                plt.bar(x, height=sizes, color=GraphUtils.COLORS[0])
            spacing = max(1, spacing)
            plt.gca().set_xticks(x[::spacing])
            plt.gca().set_xticklabels(x_labels[::spacing])
            plt.xticks(rotation=rotation)
            plt.legend(loc=legend_loc, prop={'size': legend_fontsize})
            plt.title(title)
            plt.xlabel(x_title)
            plt.ylabel(y_title)
            if axis_range is not None and 'x' in axis_range:
                new_axis_range = dict(axis_range)
                new_axis_range['x'] = (axis_range['x'][0] - 1, axis_range['x'][1] + 1)
                axis_range = new_axis_range
            GraphUtils.adjust_ranges(axis_range)
            plt.tight_layout()
            plt.savefig(file_path)
            plt.close()
        except Exception:
            logger.error('An exception was raised while creating graph: %s, (traceback below). trying to continue' % file_path)
            logger.error(traceback.format_exc())

    @staticmethod
    def pie_graph(file_path, title, labels, sizes, figsize=(PLOT_WIDTH,PLOT_HEIGHT)):
        try:
            plt.figure(figsize=figsize)
            wedges, texts = plt.pie(sizes, labels=labels, colors=GraphUtils.COLORS[:len(labels)]) # autopct='%1.2f%%',
            for text in texts:
                text.set_fontsize(8)
            for pie_wedge in wedges:
                pie_wedge.set_edgecolor('black')
                pie_wedge.set_linewidth(0.5)
            plt.gca().axis('equal')
            sum_sizes = sum(sizes)
            plt.legend(labels=['%s - %1.2f %%' % (l, s / sum_sizes * 100) for l, s in zip(labels, sizes)])
            plt.title(title)
            plt.tight_layout()
            plt.savefig(file_path)
            plt.close()
        except Exception:
            logger.error('An exception was raised while creating graph: %s , (traceback below). trying to continue' % file_path)
            logger.error(traceback.format_exc())