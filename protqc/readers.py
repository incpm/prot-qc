import re
import pandas as pd
import os
from pyteomics import mzml
from abc import ABC, abstractmethod
import datetime
if os.name == 'nt':
    from protqc.thermo import ThermoRawfile

# logging
import logging
logger = logging.getLogger(__name__)


class SpecReader(ABC):
    ZERO_TIME = datetime.datetime.utcfromtimestamp(0)

    @property
    @abstractmethod
    def total_counts(self):
        pass

    @property
    @abstractmethod
    def run_date(self):
        pass

    @abstractmethod
    def __iter__(self):
        pass

    @abstractmethod
    def __next__(self):
        pass


class Spec(ABC):
    def __init__(self, sample_name, ms_level):
        self.sample_name = sample_name
        # check ms level:
        if ms_level == 1 and (self.filter_string is None or 'Full ms' in self.filter_string):
            self._ms_level = 1
        elif ms_level == 2 and (self.filter_string is None or 'Full ms2' in self.filter_string):
            self._ms_level = 2
        else:
            self._ms_level = -1
            logger.warning("%s - Unknown ms level: %s or run mode: %s for spectrum (%s)" %
                           (sample_name, self.ms_level, self.filter_string, self.spec_id))

    @property
    def ms_level(self):
        return self._ms_level

    @property
    @abstractmethod
    def spec_id(self):
        pass

    @property
    @abstractmethod
    def scan_number(self):
        pass

    @property
    @abstractmethod
    def scan_start_time(self):
        pass

    @property
    @abstractmethod
    def injection_time(self):
        pass

    @property
    @abstractmethod
    def filter_string(self):
        pass

    @property
    @abstractmethod
    def ms2_charge_state(self):
        pass

    @property
    @abstractmethod
    def ms2_mz_value(self):
        pass


    @property
    @abstractmethod
    def intensity_array(self):
        pass

    @property
    @abstractmethod
    def mz_array(self):
        pass


class MzmlSpecReader(SpecReader):
    RUN_DATE_PATTERN = re.compile('startTimeStamp="([^"]+)"')
    TOTAL_COUNT_PATTERN = re.compile('<spectrumList[^>]*count="(\d+)"[^>]*>')

    def __init__(self, file_path):
        SpecReader.__init__(self)
        self.file_path = file_path
        self.sample_name = os.path.splitext(os.path.basename(file_path))[0]
        self._run_date = self._get_run_date(file_path)
        self._total_counts = self._get_total_counts(file_path)
        self.reader = mzml.MzML(self.file_path)

    def _get_run_date(self, path):
        with open(path) as infile:
            x = 0
            for line in infile:
                x += 1
                m = self.RUN_DATE_PATTERN.search(line)
                if m:
                    run_date_str = m.group(1)
                    run_date = (pd.to_datetime(run_date_str).tz_localize(None) - SpecReader.ZERO_TIME).total_seconds()
                    return run_date

    def _get_total_counts(self, path):
        with open(path) as infile:
            x = 0
            for line in infile:
                x += 1
                m = self.TOTAL_COUNT_PATTERN.search(line)
                if m:
                    return int(m.group(1))
        return 0

    @property
    def total_counts(self):
        return self._total_counts

    @property
    def run_date(self):
        return self._run_date

    def __iter__(self):
        self.reader.reset()
        try:
            self.spec_iter = self.reader.iterfind('spectrum')
        except Exception:
            logger.error('%s - Mzml file is corrupted or incomplete' % self.sample_name)
            raise
        return self

    def __next__(self):
        spec = MzmlSpec(self.sample_name, next(self.spec_iter))
        return spec


class MzmlSpec(Spec):
    SCAN_ID_REGEX = re.compile('scan=(\d+)')

    def __init__(self, sample_name, mzml_spec):
        self.mzml_spec = mzml_spec
        self.scan = self.mzml_spec['scanList']['scan'][0]
        Spec.__init__(self, sample_name, mzml_spec["ms level"])
        if self.ms_level == 2:
            self.selected_ion = self.mzml_spec['precursorList']['precursor'][0]['selectedIonList']['selectedIon'][0]

    @property
    def spec_id(self):
        return self.mzml_spec['id']

    @property
    def scan_number(self):
        # m = self.SCAN_ID_REGEX.search(self.spec_id)
        # scan_number = int(m.group(1))
        return int(self.mzml_spec['index']) + 1

    @property
    def scan_start_time(self):
        return self.scan['scan start time']

    @property
    def injection_time(self):
        if 'ion injection time' in self.scan:
            return self.scan['ion injection time']
        else:
            return None

    @property
    def filter_string(self):
        if 'filter string' in self.scan:
            return self.scan['filter string']
        else:
            return None

    @property
    def ms2_charge_state(self):
        if self.ms_level == 2 and 'charge state' in self.selected_ion:
            return self.selected_ion['charge state']
        else:
            return None

    @property
    def ms2_mz_value(self):
        if self.ms_level == 2:
            return self.selected_ion['selected ion m/z']
        else:
            return None

    @property
    def intensity_array(self):
        return self.mzml_spec['intensity array']

    @property
    def mz_array(self):
        return self.mzml_spec['m/z array']


class ThermoSpecReader(SpecReader):
    def __init__(self, file_path):
        SpecReader.__init__(self)
        self.file_path = file_path
        self.sample_name = os.path.splitext(os.path.basename(file_path))[0]
        self.reader = ThermoRawfile(self.file_path)
        self.first_scan_number = self.reader.GetFirstSpectrumNumber()
        self.last_scan_number = self.reader.GetLastSpectrumNumber()
        self._run_date = self._get_run_date(file_path)

    def _get_run_date(self, path):
        run_date = (self.reader.GetCreationDate() - self.ZERO_TIME).total_seconds()
        return run_date

    @property
    def total_counts(self):
        return self.last_scan_number

    @property
    def run_date(self):
        return self._run_date

    def __iter__(self):
        self.current_scan_number = self.first_scan_number
        return self

    def __next__(self):
        if self.current_scan_number > self.last_scan_number:
            raise StopIteration
        spec = ThermoSpec(self.sample_name, self.reader, self.current_scan_number)
        self.current_scan_number += 1
        return spec

    def __del__(self):
        try:
            if self.reader:
                self.reader.Close()
        except:
            pass


class ThermoSpec(Spec):
    def __init__(self, sample_name, reader, scan_number):
        self.reader = reader
        self._scan_number = scan_number
        self.prec_info = self.reader.GetPrecursorInfoFromScanNumFixed(self.scan_number)
        self._mz_array, self._intensity_array = self.reader.ScanArrays(self.scan_number)
        Spec.__init__(self, sample_name, reader.GetMSOrderForScanNum(scan_number))

    @property
    def spec_id(self):
        return 'scan=%s' % self.scan_number

    @property
    def scan_number(self):
        return self._scan_number

    @property
    def scan_start_time(self):
        return self.reader.RTFromScanNum(self.scan_number)

    @property
    def injection_time(self):
        return self.prec_info['injection time']

    @property
    def filter_string(self):
        return self.reader.GetFilterForScanNum(self.scan_number)

    @property
    def ms2_charge_state(self):
        if self.ms_level == 2:
            return self.prec_info['charge']
        else:
            return None

    @property
    def ms2_mz_value(self):
        if self.ms_level == 2:
            return self.prec_info['mz']
        else:
            return None

    @property
    def intensity_array(self):
        return self._intensity_array

    @property
    def mz_array(self):
        return self._mz_array


