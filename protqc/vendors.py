import os
import sys

VENDORS_EXT_DICT = {
        'thermo': '.raw',
        'agilent/brooker': '.d',
        'syes': '.wiff'
    }
SUPPORTED_VENDORS = ['thermo']

THERMO_DEFAULT_32_DLL_PATHS = [
    # Theoretical Bundled 32 bit MSFileReader DLL
    os.path.join(os.path.dirname(os.path.abspath(__file__)), 'XRawfile2.dll'),
    # Default Installation Path on 64 bit systems
    u'C:\\Program Files (x86)\\Thermo\\MSFileReader\\XRawfile2.dll',
    u'D:\\Program Files (x86)\\Thermo\\MSFileReader\\XRawfile2.dll',
    # Default Installation Path on 32 bit systems
    u'C:\\Program Files\\Thermo\\MSFileReader\\XRawfile2.dll',
    u'D:\\Program Files\\Thermo\\MSFileReader\\XRawfile2.dll',
]

THERMO_DEFAULT_64_DLL_PATHS = [
    # Theoretical Bundled 64 bit MSFileReader DLL
    os.path.join(os.path.dirname(os.path.abspath(__file__)), 'XRawfile2_x64.dll'),
    # Default Installation Path on 64 bit systems
    u'C:\\Program Files\\Thermo\\MSFileReader\\XRawfile2_x64.dll',
    u'D:\\Program Files\\Thermo\\MSFileReader\\XRawfile2_x64.dll',
]


def get_file_vendor(file_path):
    for vendor in VENDORS_EXT_DICT:
        if file_path.endswith(VENDORS_EXT_DICT[vendor]):
            return vendor
    return None


def get_thermo_dll_paths():
    if sys.maxsize > 2**32:
        return THERMO_DEFAULT_64_DLL_PATHS
    else:
        return THERMO_DEFAULT_32_DLL_PATHS


def is_thermo_supported():
    return os.name == 'nt' and any([os.path.exists(path) for path in get_thermo_dll_paths()])
