import json
import importlib
from protqc.settings import VERSION

# logging
import logging
logger = logging.getLogger(__name__)


# class to make objects jsonable
class JSONable:
    def __init__(self, keep_params=None):
        self.keep_params = [] if keep_params is None else keep_params

    def _to_json_hook(self):
        json_repr = dict(cls=self.__class__.__name__)
        json_repr.update(dict([(k, v) for (k, v) in self.__dict__.items()
                               if k in self.keep_params]))
        return json_repr

    def to_json_file(self, file_path):
        with open(file_path, 'w') as fp:
            json.dump(obj={'version': VERSION, 'data': self}, fp=fp, default=JSONHooks.json_dump_default, indent=2, sort_keys=True)

    @classmethod
    def _from_json_hook(cls, dct):
        instance_params = dict(dct)
        del instance_params['cls']
        try:
            return cls(**instance_params)
        except Exception:
            logger.error('failed during from_json() with class %s' % (dct['cls'],))
            raise

    @staticmethod
    def from_json_file(file_path):
        with open(file_path, 'r') as fp:
            obj = json.load(fp=fp, object_hook=JSONHooks.json_load_object_hook)
            if type(obj) is dict and 'version' in obj:
                json_version = obj['version']
                data = obj['data']
            else:
                json_version = 'no_version'
                data = obj
            if json_version != VERSION:
                logger.warning('Incompatible versions - json file(%s) is version \'%s\' and you are using version \'%s\''
                               ', if you are experiencing weird problems, please rerun again from raw or mzml'
                               % (file_path, json_version, VERSION))
        return data


class JSONCompareUtils(object):
    @staticmethod
    def _is_equal(x1, x2, float_delta):
        types = [type(x1), type(x2)]
        if float in types and int in types:
            x1 = float(x1)
            x2 = float(x2)
        if type(x1) is not type(x2):
            return False
        if type(x1) is float:
            return abs(x1 - x2) < float_delta
        return x1 == x2

    @staticmethod
    def compare_json_data(source_data_a, source_data_b, float_delta):
        def compare(data_a, data_b):
            # type - list
            if type(data_a) is list:
                # is [data_b] a list and of same length as [data_a]?
                if (
                            (type(data_b) != list) or
                            (len(data_a) != len(data_b))
                ):
                    return False

                # iterate over list items
                for list_index, list_item in enumerate(data_a):
                    # compare [data_a] list item against [data_b] at index
                    if not compare(list_item, data_b[list_index]):
                        return False

                # list identical
                return True

            # type - dictionary
            if type(data_a) is dict:
                # is [data_b] a dictionary?
                if type(data_b) != dict:
                    return False

                # iterate over dictionary keys
                for dict_key, dict_value in data_a.items():
                    # key exists in [data_b] dictionary, and same value?
                    if (
                                (dict_key not in data_b) or
                                (not compare(dict_value, data_b[dict_key]))
                    ):
                        return False

                # dictionary identical
                return True

            # simple value - compare both value and type for equality
            return JSONCompareUtils._is_equal(data_a, data_b, float_delta)

        # compare a to b, then b to a
        return (
            compare(source_data_a, source_data_b) and
            compare(source_data_b, source_data_a)
        )


# JSON save/load helper functions
class JSONHooks(object):
    @staticmethod
    def json_dump_default(obj):
        if isinstance(obj, set):
            return list(obj)
        elif '_to_json_hook' in dir(obj):
            json_repr = obj._to_json_hook()
            return json_repr
        elif obj.__class__.__name__ == 'float32':
            return float(obj)
        else:
            return obj

    @staticmethod
    def _get_class_obj(dct, module_name, cls_name):
        try:
            mdl = importlib.import_module(module_name)
            cls = getattr(mdl, cls_name)
            if '_from_json_hook' in dir(cls):
                return cls._from_json_hook(dct)
        except (ImportError, AttributeError) as e:
            return None

    @staticmethod
    def json_load_object_hook(dct):
        if 'cls' in dct:
            obj = JSONHooks._get_class_obj(dct, 'protqc.modules', dct['cls'])
            if obj is None:
                obj = JSONHooks._get_class_obj(dct, 'protqc.data', dct['cls'])
            if obj is not None:
                return obj
        return dct


def load_json_dict(file_path):
    # open JSON file and parse contents
    fh = open(file_path, 'r')
    data = json.load(fh)
    fh.close()
    return data


def compare_json_files(file_path1, file_path2, float_delta=0.001):
    a_json = load_json_dict(file_path1)
    b_json = load_json_dict(file_path2)
    return JSONCompareUtils.compare_json_data(a_json, b_json, float_delta)


def compare_qc_data_json_files(file_path1, file_path2, float_delta=0.001):
    a_json = load_json_dict(file_path1)
    b_json = load_json_dict(file_path2)
    return JSONCompareUtils.compare_json_data(a_json['data'], b_json['data'], float_delta)


