# logging
import logging
import os
from typing import List

import dask
import dask.multiprocessing
import dask.threaded

from protqc.data import SampleQCData
from protqc.modules import COMPONENTS_DICT, ID_COMPONENT_DICT, initialize_components, MASS_DEVIATION
from protqc.multiprocessing import start_message_queue, STOP_THREAD, get_child_logger
from protqc.readers import MzmlSpecReader, ThermoSpecReader
from protqc.vendors import is_thermo_supported

logger = logging.getLogger(__name__)


class QCAnalyzer(object):
    def __init__(self, input, output_dir, id_dir=None, masses=None):
        # get mzML files:
        self.sample_names = set()
        self.output_dir = output_dir
        self.masses = masses
        self.mzml_dict = {}
        self.json_dict = {}
        self.raw_dict = {}
        self.id_dir_dict = {}
        if id_dir:
            id_dir = os.path.abspath(id_dir)
            id_dirs = os.listdir(id_dir)
            self.id_dir_dict.update(
                {x: os.path.join(id_dir, x) for x in id_dirs})
        if type(input) is str:
            input = [input]
        for current_input in input:
            current_input = os.path.abspath(current_input)
            # if it is a folder:
            if os.path.isdir(current_input):
                files = os.listdir(current_input)
                self.sample_names.update([os.path.splitext(x)[0] for x in files
                                      if x.lower().endswith('.mzml') or x.lower().endswith('.json') or x.lower().endswith('.raw')])
                self.mzml_dict.update({os.path.splitext(x)[0]: os.path.join(current_input, x)
                                       for x in files if x.lower().endswith('.mzml')})
                self.json_dict.update({os.path.splitext(x)[0]: os.path.join(current_input, x)
                                       for x in files if x.lower().endswith('.json')})
                self.raw_dict.update({os.path.splitext(x)[0]: os.path.join(current_input, x)
                                        for x in files if x.lower().endswith('.raw')})
            # if it is a file:
            else:
                sample_name = os.path.splitext(os.path.basename(current_input))[0]
                self.sample_names.add(sample_name)
                if current_input.lower().endswith('.mzml'):
                    self.mzml_dict[sample_name] = current_input
                elif current_input.lower().endswith('.json'):
                    self.json_dict[sample_name] = current_input
                elif current_input.lower().endswith('.raw'):
                    self.raw_dict[sample_name] = current_input
                else:
                    raise Exception('Unknown input file format: %s' % current_input)
        self.sample_names = list(self.sample_names)
        # input validation:
        if not self.sample_names:
            raise Exception('Can\'t find any raw/mzML/JSON files in given input.')
        logger.info('Found %s samples to analyze' % len(self.sample_names))


    @staticmethod
    def _get_appropriate_reader(sample_name, mzml_dict, raw_dict):
        # decide which reader to use (mzML or Thermo raw):
        if sample_name in raw_dict and is_thermo_supported():
            return ThermoSpecReader, raw_dict[sample_name]
        elif sample_name in mzml_dict:
            return MzmlSpecReader, mzml_dict[sample_name]
        else:
            if os.name == 'nt' and sample_name in raw_dict:
                raise Exception(
                    '%s - Can\'t read from raw file, msfilereader DLL must be installed.' % sample_name)
            else:
                raise Exception(
                    '%s - No mzML found for sample (raw file reading is supported only for windows after installing msfilereader DLL)' % sample_name)

    @staticmethod
    def _analyze_sample(sample_name, output_dir, json_dict, mzml_dict, raw_dict, id_dict, masses, components_list: List[str], queue=None):
        # if multiprocessing - do not move this logging part!:
        logger = get_child_logger(queue, __name__)
        # process sample:
        logger.info("%s - Analyzing sample..." % sample_name)
        components_to_do = set(components_list)
        sample_qc = None
        json_path = os.path.join(output_dir, '%s.json' % sample_name)
        if sample_name in json_dict:
            logger.info('%s - Found json file for sample' % sample_name)
            sample_qc = SampleQCData.from_json_file(json_dict[sample_name])
            # fix incase json file was renamed.
            sample_qc.sample = sample_name
            components_to_do = components_to_do - set(sample_qc.components_dict)
            if len(components_to_do) == 0:
                logger.info('%s - Skipping processing for sample' % sample_name)
                logger.info('%s - Saving json file: %s' % (sample_name, json_path))
                sample_qc.to_json_file(json_path)
                return json_path
            else:
                try:
                    reader_cls, file_path = QCAnalyzer._get_appropriate_reader(sample_name, mzml_dict, raw_dict)
                except Exception:
                    raise Exception('%s - trying to load json file but it is outdated, you need to also put an mzML/raw file in order to complete the missing parts' % sample_name)
        else:
            # get the appropriate reader class and file:
            reader_cls, file_path = QCAnalyzer._get_appropriate_reader(sample_name, mzml_dict, raw_dict)
        # process the mzml file:
        logger.info('%s - Loading file: %s' % (sample_name, file_path))
        spec_reader = reader_cls(file_path)
        # initialize components:
        logger.info('%s - Initializing components: %s' % (sample_name, components_to_do))
        components_params_dict = {'sample_name': sample_name, 'run_date': spec_reader.run_date, 'masses': masses, 'total_counts': spec_reader.total_counts, 'id_folder': id_dict.get(sample_name)}
        initialized_components = initialize_components(list(components_to_do), components_params_dict)

        # read data:
        spec_id = None
        logger.info('%s - Starting to process file...' % sample_name)
        logger.info('%s - total number of scans: %s' % (sample_name, spec_reader.total_counts))
        try:
            for spec in spec_reader:
                spec_id = spec.spec_id
                for component in initialized_components:
                    initialized_components[component].read_spec(spec)
                if spec.scan_number % 500 == 0:
                    logger.info('%s - scan number: %s' % (sample_name, spec.scan_number))
        except Exception:
            logger.error('%s - Error while processing file for sample, scan id: %s' % (sample_name, spec_id))
            raise
        # wrap up components:
        for component in initialized_components:
            initialized_components[component].finish()

        # create sample protqc object:
        if sample_qc is not None:
            initialized_components.update(sample_qc.components_dict)
        sample_qc = SampleQCData(sample_name, initialized_components)
        logger.info('%s - Saving json file: %s' % (sample_name, json_path))
        sample_qc.to_json_file(json_path)
        logger.info('%s - Finished processing file' % sample_name)
        return json_path

    def analyze(self, num_workers=1, components_list=None):
        # handle multiprocessing logging:
        scheduler, queue = start_message_queue(num_workers=num_workers)
        # all samples data init:
        logger.info("Analyzing QC Data...")
        if components_list is None:
            components_list = list(COMPONENTS_DICT.keys())
        # remove mass deviation component if no masses where provided:
        if not self.masses:
            components_list.remove(MASS_DEVIATION)

        samples_dict = {}
        # iterate over the different samples and save a delayed analyze function for later parallel processing:
        for sample_name in self.sample_names:
            id_components = []
            if sample_name in self.id_dir_dict:
                id_components = list(ID_COMPONENT_DICT.keys())
            samples_dict[sample_name] = dask.delayed(QCAnalyzer._analyze_sample)(sample_name, self.output_dir, self.json_dict, self.mzml_dict, self.raw_dict,
                                                                                 self.id_dir_dict, self.masses, components_list + id_components, queue)
        # process all samples in parallel and get jsons path (if needed):
        try:
            samples_dict = dask.compute(samples_dict, scheduler=scheduler, num_workers=num_workers)[0]
        except Exception:
            raise
        finally:
            # end queue messages thread if needed:
            if queue:
                queue.put(STOP_THREAD)
        return samples_dict