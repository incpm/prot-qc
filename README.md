# RawBeans

## Documentation

can be found [here](https://incpmpm.atlassian.net/wiki/spaces/PUB/pages/103290936/Raw+Beans)

## Installation

This package requires python 3.5 - 3.8.1(note that python 3.9 currently does problems, the python needs to be 32-bit if you are using the msfilereader 32-bit DLL), To install, download the source code and run:

    python setup.py install

## Usage
    (prot-qc) [pmrotem@isrv02 prot-qc]$ create-qc-report.py -h
    usage: create-qc-report.py [-h] [--input INPUT [INPUT ...]] --output-dir
                               OUTPUT_DIR [--cores CORES] [--batch]
                               [--masses MASSES [MASSES ...]]
    
    Creates a Proteomics QC Report
    
    optional arguments:
      -h, --help            show this help message and exit
      --input INPUT [INPUT ...]
                            mzML/raw/d/wiff files or folders that contain the
                            previous formats separated by space.
      --output-dir OUTPUT_DIR
                            Output folder, were the report will be written.
      --cores CORES         Number of processes to use, Default: 1
      --batch               Toggle QC Report mode, if True - one report for each
                            sample, if False - one reports to all samples.
                            Default: off
      --masses MASSES [MASSES ...]
                            List of masses to follow, separated by space. Default:
                            [371.10124, 445.12003]


## Output

*create-qc-report.py* generates several output files inside the output directory.

qc-report.html - the qc report with interactive graphs.

static-qc-report.html - the qc report with static graphs (as images) .

json files - for each sample a json file will be written, you can use it as input next time you run this sample and it will skip processing phase.

resource folder - images, javascript and css.

## Tests

To run unit tests, use:

    nosetests

Or using tox:

    tox

Command line example:

    create-qc-report.py --input /path/to/mzml_file.mzML --output-dir /path/to/output_dir

Dockerfile example:

    FROM python:3.8-buster
    RUN curl -L -o /rawbeans.zip https://bitbucket.org/incpm/prot-qc/get/769471980759.zip && \
        unzip /rawbeans.zip -d /src && \
        cd /src/incpm-prot-qc-769471980759/ && \
        python setup.py install
    CMD ["create-qc-report.py"]
