import os
from setuptools import setup, find_packages


requires = ['scipy==1.5.2', 'numpy==1.19.1', 'pandas==1.1.0', 'matplotlib==3.2.2', 'seaborn==0.10.1', 'jinja2==3.0.0a1',
            'lxml==4.5.2', 'pyteomics==4.3.2', 'pandas==1.1.0', 'comtypes==1.1.7', 'dask[complete]==2.22.0', 'distributed==2.4.0']
if os.name == 'nt':
    requires.append('wxPython==4.1.0')

setup(
    name='protqc',
    version='1.6.5',
    author='INCPM',
    author_email='incpmdevel@gmail.com',
    packages=find_packages(),
    url='https://bitbucket.org/incpm/prot-qc',
    scripts=['scripts/create-qc-report.py', 'scripts/qc-report-gui.py', 'scripts/print-spectrum.py',
             'scripts/test-script.py', 'scripts/get-prec-ratio-scans.py'],
    description='Python code for Proteomics QC',
    long_description=open('README.md').read(),
    tests_require=requires + ['nose'],
    install_requires=requires,
    test_suite='nose.collector',
    include_package_data=True
)
